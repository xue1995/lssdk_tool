##Compile Command
```
环境：      python 3.6.8
编译：      .\make.bat main.py
安装命令:   pip install xxx
依赖：PySide2, pyserial, pyinstaller, pylint, 其余根据提示安装。

帮助：
1. 如何让designer.exe生成ui.py?
    ① 操作方法： 菜单栏 -- 窗体 -- View Python Code -- 保存(图标)
    ② 创造环境:  \Python\Python36\Lib\site-packages\PySide2\bin
                [bin文件夹需要手动创建]，并放入如下文件：
                                        [Qt5Core.dll] [uic.exe]
                所需文件皆在 *\site-packages\PySide2\
    ③ 绑定ui文件以[designer.exe]打开。
      ui文件右键--打开方式--选择designer.exe打开。

2. VSCode常用插件：
    提供了代码分析,高亮,规范化等很多基本功能,    Python
    路径自动提示,                              Path Autocomplete
    括号自动变色,                              Bracket Pair Colorizer
    文件图标,                                 vscode-icons
    自动格式化代码(请选择局部内容格式化),        autopep8/yapf
    错误提示,                                  flakes8

3. configparser模块的使用:
    https://www.cnblogs.com/imyalost/p/8857896.html

4. 给pyinstaller配置upx压缩功能
    环境:  \Python\Python36\Lib\site-packages\PyInstaller
    准备： upx.exe    (可以自行下载或在从群共享获取)
    操作：将[upx.exe]放入环境目录中
```
