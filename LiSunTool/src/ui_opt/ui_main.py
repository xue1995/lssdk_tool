from PySide2.QtCore import QPoint
from PySide2.QtWidgets import QFileDialog, QMainWindow, QMessageBox
from src.app.app_config import AppConfig
from src.app.app_port import AppPort
from ui.ui_lisun import Ui_MainWindow


class MainWindow(QMainWindow):
    Cfg = AppConfig()

    def __init__(self):
        super(MainWindow, self).__init__()
        self.MainWindow = Ui_MainWindow()
        self.MainWindow.setupUi(self)
        # 注册串口类(附带4个页面操作user/debug/char)
        self.Port = AppPort(self)

        self.__init()
        ''' 设置主窗口控件 '''
        # 设置始终显示第0页
        self.MainWindow.tab_func.setCurrentIndex(0)
        self.curr_page = 1
        # # 设置表格列宽
        # self.MainWindow.tbw_loopDebugAT.setColumnWidth(0, 190)

    def __init(self):
        # 读取配置文件
        self.Cfg.readAllCfg(self.MainWindow)

        # Serial Func
        self.Port.uiPortCheck()  # 刷新下拉列表显示串口信息
        self.MainWindow.btn_scanSerial.clicked.connect(self.Port.uiPortCheck)
        self.MainWindow.btn_switchSerial.clicked.connect(self.Port.uiPortSwitch)

        # 绑定页切换
        self.MainWindow.tab_func.currentChanged.connect(self.tab_func_page)

        # Debug Func
        self.MainWindow.btn_debugSend.clicked.connect(
            self.Port.Debug.uiSendData)

        self.MainWindow.pte_debugReceive.textChanged.connect(
            lambda: self.textChanged(self.MainWindow.pte_debugReceive))
        self.MainWindow.btn_debugCleanReceiveText.clicked.connect(
            lambda: self.MainWindow.pte_debugReceive.setPlainText(""))
        self.MainWindow.btn_debugCleanSendText.clicked.connect(
            lambda: self.MainWindow.pte_debugSend.setPlainText(""))

        self.MainWindow.btn_debugSaveReceiveText.clicked.connect(
            lambda: self.saveReceiveText(self.MainWindow.pte_debugReceive.
                                         toPlainText()))

        self.MainWindow.lvw_debugAT.itemClicked.connect(
            self.Port.Debug.getAtCmd)
        self.MainWindow.lvw_debugAT.customContextMenuRequested[QPoint].connect(
            self.Port.Debug.atMenu)

        # LoopDebug
        self.MainWindow.btn_loopDebugStart.clicked.connect(
            self.Port.Debug.uiLoopSwitch)

        self.MainWindow.btn_loopDebugFull.clicked.connect(
            self.Port.Debug.uiLoopAtFull)
        self.MainWindow.btn_loopDebugNop.clicked.connect(
            self.Port.Debug.uiLoopAtNop)

        self.MainWindow.btn_loopAddLine.clicked.connect(self.Port.Debug.uiLoopAtAddLine)
        self.MainWindow.btn_loopDelLine.clicked.connect(self.Port.Debug.uiLoopAtDelLine)

        self.MainWindow.tbw_loopDebugAT.customContextMenuRequested[
            QPoint].connect(self.Port.Debug.uiLoopAtMenu)

        # UP
        self.MainWindow.brn_upFilePath.clicked.connect(
            self.Port.Dfu.uiReadUpFile)
        self.MainWindow.brn_upStartUpgrade.clicked.connect(
            self.Port.Dfu.uiTaskSwitch)

        # 串口线程
        self.Port.delivery_signal.connect(self.uiRefreshReceive)
        self.Port.upBar_signal.connect(self.uiUpBar)
        self.Port.msg_signal.connect(self.uiMessageBox)
        self.Port.lampCfg_signal.connect(self.Port.User.uiRefreshLampCfg)

        # LED参数配置
        self.MainWindow.btn_readConfig.clicked.connect(
            self.Port.User.uiReadConfig)
        self.MainWindow.btn_writeConfig.clicked.connect(
            self.Port.User.uiWriteConfig)
        self.MainWindow.btn_refreshLampInfo.clicked.connect(
            self.Port.User.uiRefreshLampInfo)

        self.MainWindow.btn_addMode.clicked.connect(
            self.Port.User.uiLampCfgAddMode)
        self.MainWindow.btn_deleteMode.clicked.connect(
            self.Port.User.uiLampCfgDeleteMode)

        self.MainWindow.btn_addGear.clicked.connect(
            self.Port.User.uiLampCfgAddGear)
        self.MainWindow.btn_deleteGear.clicked.connect(
            self.Port.User.uiLampCfgDeleteGear)

        # 工厂烧录
        self.MainWindow.brn_jlinkFindPath.clicked.connect(
            self.Port.FactoryBurn.uiJlinkFindFile)
        self.MainWindow.brn_jlinkFolderPath.clicked.connect(
            self.Port.FactoryBurn.uiReadJlinkFile)
        self.MainWindow.brn_jlinkBurnFilePath.clicked.connect(
            self.Port.FactoryBurn.uiReadJlinkBurnFile)
        self.MainWindow.brn_jlinkStartBurnFile.clicked.connect(
            self.Port.FactoryBurn.uiStartJlinkBurnFile)
        self.MainWindow.brn_jlinkChipUnLock.clicked.connect(
            self.Port.FactoryBurn.uiUnJlinkLock)

        self.MainWindow.brn_stlinkFindPath.clicked.connect(
            self.Port.FactoryBurn.uiSTlinkFindFile)
        self.MainWindow.brn_stlinkFolderPath.clicked.connect(
            self.Port.FactoryBurn.uiReadSTlinkFile)
        self.MainWindow.brn_stlinkBurnFilePath.clicked.connect(
            self.Port.FactoryBurn.uiReadSTlinkBurnFile)
        self.MainWindow.brn_stlinkStartBurnFile.clicked.connect(
            self.Port.FactoryBurn.uiStartSTlinkBurnFile)

    def closeEvent(self, event):
        # 关闭线程
        self.Port.closePort()

        # 保存配置
        self.Cfg.saveAllCfg(self.MainWindow)


    def saveReceiveText(self, text):
        if len(text) < 1:
            return

        file_name = []
        file_name = QFileDialog.getSaveFileName(self, "文件保存", "/",
                                                "Text Files(*.txt)")
        if len(file_name[0]) < 1:
            return

        with open(file=file_name[0], mode='w+', encoding='utf-8') as f:
            f.write(text)
            f.close()

    def textChanged(self, controls):
        controls.moveCursor(controls.textCursor().End)  # 文本框显示到底部

    def tab_func_page(self):
        self.curr_page = self.MainWindow.tab_func.currentIndex() + 1

    def uiRefreshReceive(self, page, text):
        if (page == 1):
            self.MainWindow.pte_debugReceive.appendPlainText(text)
        elif (page == 2):
            self.MainWindow.pte_debugReceive.appendPlainText(text)
        elif (page == 3):
            self.MainWindow.pte_debugReceive.appendPlainText(text)

    def uiUpBar(self, val):
        self.MainWindow.prg_upBar.setValue(val)

    def uiMessageBox(self, type, title, text):
        if (type == 0):
            QMessageBox.information(self, title, text)
        elif (type == 1):
            QMessageBox.question(self, title, text)
        elif (type == 2):
            QMessageBox.warning(self, title, text)
        elif (type == 3):
            QMessageBox.critical(self, title, text)
        elif (type == 4):
            QMessageBox.about(self, title, text)
