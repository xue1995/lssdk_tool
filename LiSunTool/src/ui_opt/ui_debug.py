from PySide2.QtCore import Signal
from PySide2.QtWidgets import QDialog, QMessageBox
from ui.ui_debug_at import Ui_DebugAt


class DebugWindow(QDialog):

    text_singal = Signal(int, str, str)
    index = 0

    def __init__(self):
        super(DebugWindow, self).__init__()
        self.ui = Ui_DebugAt()
        self.ui.setupUi(self)
        self.init()

    def init(self):
        self.ui.brn_cancel.clicked.connect(self.close)
        self.ui.brn_confirm.clicked.connect(self.confirm)

    def confirm(self):
        name = self.ui.le_name.text()
        if(name == ""):
            QMessageBox.critical(self.ui, "Error", u"名称不能为空")
            return

        cmd = self.ui.le_cmd.text()
        if(cmd == "" and cmd == " "):
            cmd = "无"

        hint = self.ui.pte_hint.toPlainText()
        if(hint == "" and hint == " "):
            hint = "无"

        self.text_singal.emit(
            self.index, self.ui.le_name.text(),
            cmd + "\n\n" + hint)
        self.close()
