import os  # 引入操作系统模块
import sys
import string

all_search_file = []

def get_disk():
    disk_list = []

    for disk in string.ascii_uppercase:
        disk = disk + ':\\'
        if os.path.exists(disk):
            disk_list.append(disk)

    return disk_list

# 搜索包含关键字的目录
def searchDirs(root, name, dirs):
    for _dir in dirs:
        if -1 != _dir.find(name) and _dir[0] != '$':
            AllDir = os.path.join(root, _dir)
            all_search_file.append(AllDir)


# 搜索包含关键字的文件
def searchFiles(root, name, files):
    for file in files:
        if name in file:  # '$' not in AllFile 去除隐藏文件
            AllFile = os.path.join(root, file)
            all_search_file.append(AllFile)
            print(AllFile)


def search(name):
    curdisks = get_disk()
    try:
        for disk in curdisks:
            for root, dirs, files in os.walk(disk, True):
                searchDirs(root, name, dirs)
                searchFiles(root, name, files)
    except:
        pass

# 搜索包含关键字的文件 找到一个就返回
def searchFiles_s(root, name, files):
    for file in files:
        if name in file:  # '$' not in AllFile 去除隐藏文件
            AllFile = os.path.join(root, file)
            print(AllFile)
            return AllFile
    return ""

def search_file(name):
    curdisks = get_disk()
    try:
        for disk in curdisks:
            for root, dirs, files in os.walk(disk, True):
                path = searchFiles_s(root, name, files)
                if path != "":
                    return path
    except:
        return ""
