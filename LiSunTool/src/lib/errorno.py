
g_errno_d = {
    'OK':             0,
    'FAIL':           1,      # Failure
    'NULL':           2,      # Invalid argument
    'BUSY':           3,      # Device or resource busy
    'STATE':          4,      # Unexpected
    'TIMEOUT':        5,      # Timeout
    'BUS':            6,      # Bus error
    'SEND':           7,      # Send failure
    'RECV':           8,      # Receive failure
    'OPEN':           9,      # Open failure
    'CLOSE':          10,     # Close failure
    'GET':            11,     # Get failure
    'SET':            12,     # Set failure
    'RANGE':          13,     # Number out of range
    'FULL':           14,     # Full
    'EMPTY':          15,     # Empty
    'ACCESS':         16,     # Permission denied
    'LOCKED':         17,     # Device or resource is locked
    'COMM':           18,     # Communication error
    'PROTO':          19,     # Protocol error
    'MSG':            20,     # Bad message
    'CRC':            21,     # checksum error
    'SYNC':           22,     # Synchronization failure
    'ALLOC':          23,     # Allocation failure
    'FRAME':          24,     # Error frame
    'FORMAT':         25,     # format error
    'OVERLOAD':       26,     # Overload
    'NOT_SUPPORT':    27,     # Not support
    'NOT_EMPTY':      28,     # Not empty
    'NOT_CONN':       29,     # Not connected
    'INVAL_PARM':     30,     # Invalid parameter
    'INVAL_DATA':     31,     # Invalid data
    'INVAL_LEN':      32,     # Invalid length
    'INVAL_ADDR':     33,     # Invalid address
    'INVAL_CMD':      34,     # Invalid command
    'NO_DEV':         35,     # No such device
    'NO_SPACE':       36,     # No Space
    'NO_MEM':         37,     # No memory
}
