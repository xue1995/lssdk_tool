import os
import sys

from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QApplication, QStyleFactory
from PySide2.QtCore import QCoreApplication, Qt
from src.ui_opt.ui_main import MainWindow
from src.lib.resource_path import resource_path

if __name__ == "__main__":
    QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling)

    app = QApplication(sys.argv)
    QApplication.setStyle(QStyleFactory.create('Fusion'))
    main_window = MainWindow()
    path = resource_path(r'res\LiSun.ico')
    main_window.setWindowIcon(QIcon(path))
    path = resource_path(r'ui\qss\black.qss')
    with open(path) as file:
        str = file.readlines()
        str = ''.join(str).strip('\n')
        app.setStyleSheet(str)
    main_window.show()
    sys.exit(app.exec_())
