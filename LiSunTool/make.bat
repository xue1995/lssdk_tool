@echo off
::set p0=%1
set p0=.\main.py
echo %p0%

::echo -F 打包一个单个文件  -D 打包多个文件  -icon=路径  -v FILE
::echo --hidden-import 应用需要的包，但是没有被打包进来  
::echo --upx-dir C:\upx3.96\
::echo -w --noconsole 去除调试黑窗口
::echo --upx-exclude 排除需要压缩的文件

pyinstaller --add-data="res\LiSun.ico;res" --add-data="ui\qss\black.qss;ui\qss" --add-data="ui\qss\black_rc\*.png;ui\qss\black_rc" -n LiSunTool -F %p0% -i=.\res\LiSun.ico --clean --upx-exclude=msvcp140.dll --upx-exclude=msvcp140_1.dll --upx-exclude=vcruntime140.dll --upx-exclude=python36.dll --upx-exclude=vcruntime140_1.dll --upx-exclude=qwindowsvistastyle.dll --upx-exclude=qwindows.dll --upx-exclude=qwebgl.dll --upx-exclude=qoffscreen.dll --upx-exclude=qminimal.dll --upx-exclude=qico.dll -w --noconsole


pause
exit