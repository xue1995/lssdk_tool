# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'debug_atjBVZTU.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_DebugAt(object):
    def setupUi(self, DebugAt):
        if not DebugAt.objectName():
            DebugAt.setObjectName(u"DebugAt")
        DebugAt.resize(253, 201)
        DebugAt.setMinimumSize(QSize(253, 201))
        DebugAt.setMaximumSize(QSize(413, 302))
        font = QFont()
        font.setFamily(u"\u5b8b\u4f53")
        DebugAt.setFont(font)
        self.gridLayout_5 = QGridLayout(DebugAt)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.lbl_name = QLabel(DebugAt)
        self.lbl_name.setObjectName(u"lbl_name")
        font1 = QFont()
        font1.setFamily(u"\u5b8b\u4f53")
        font1.setPointSize(11)
        self.lbl_name.setFont(font1)

        self.gridLayout.addWidget(self.lbl_name, 0, 0, 1, 1)

        self.le_name = QLineEdit(DebugAt)
        self.le_name.setObjectName(u"le_name")
        self.le_name.setFont(font1)

        self.gridLayout.addWidget(self.le_name, 0, 1, 1, 1)


        self.gridLayout_5.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.lbl_cmd = QLabel(DebugAt)
        self.lbl_cmd.setObjectName(u"lbl_cmd")
        self.lbl_cmd.setFont(font1)

        self.gridLayout_2.addWidget(self.lbl_cmd, 0, 0, 1, 1)

        self.le_cmd = QLineEdit(DebugAt)
        self.le_cmd.setObjectName(u"le_cmd")
        self.le_cmd.setFont(font1)

        self.gridLayout_2.addWidget(self.le_cmd, 0, 1, 1, 1)


        self.gridLayout_5.addLayout(self.gridLayout_2, 1, 0, 1, 1)

        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.lbl_hint = QLabel(DebugAt)
        self.lbl_hint.setObjectName(u"lbl_hint")
        self.lbl_hint.setFont(font1)

        self.gridLayout_3.addWidget(self.lbl_hint, 0, 0, 1, 1)


        self.gridLayout_5.addLayout(self.gridLayout_3, 2, 0, 1, 1)

        self.pte_hint = QPlainTextEdit(DebugAt)
        self.pte_hint.setObjectName(u"pte_hint")

        self.gridLayout_5.addWidget(self.pte_hint, 3, 0, 1, 1)

        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.brn_confirm = QPushButton(DebugAt)
        self.brn_confirm.setObjectName(u"brn_confirm")
        self.brn_confirm.setFont(font1)

        self.gridLayout_4.addWidget(self.brn_confirm, 0, 0, 1, 1)

        self.brn_cancel = QPushButton(DebugAt)
        self.brn_cancel.setObjectName(u"brn_cancel")
        self.brn_cancel.setFont(font1)

        self.gridLayout_4.addWidget(self.brn_cancel, 0, 1, 1, 1)


        self.gridLayout_5.addLayout(self.gridLayout_4, 4, 0, 1, 1)


        self.retranslateUi(DebugAt)

        QMetaObject.connectSlotsByName(DebugAt)
    # setupUi

    def retranslateUi(self, DebugAt):
        DebugAt.setWindowTitle(QCoreApplication.translate("DebugAt", u"AT\u547d\u4ee4", None))
        self.lbl_name.setText(QCoreApplication.translate("DebugAt", u"\u540d\u79f0:", None))
#if QT_CONFIG(tooltip)
        self.le_name.setToolTip(QCoreApplication.translate("DebugAt", u"\u6bd4\u5982\uff1a\u67e5\u8be2\u7248\u672c", None))
#endif // QT_CONFIG(tooltip)
        self.lbl_cmd.setText(QCoreApplication.translate("DebugAt", u"\u547d\u4ee4:", None))
#if QT_CONFIG(tooltip)
        self.le_cmd.setToolTip(QCoreApplication.translate("DebugAt", u"\u6bd4\u5982\uff1aAT^VERSION", None))
#endif // QT_CONFIG(tooltip)
        self.lbl_hint.setText(QCoreApplication.translate("DebugAt", u"\u63d0\u793a:", None))
#if QT_CONFIG(tooltip)
        self.pte_hint.setToolTip(QCoreApplication.translate("DebugAt", u"\u4ecb\u7ecd\u547d\u4ee4\u4f7f\u7528\u65b9\u6cd5", None))
#endif // QT_CONFIG(tooltip)
        self.brn_confirm.setText(QCoreApplication.translate("DebugAt", u"\u786e\u5b9a", None))
        self.brn_cancel.setText(QCoreApplication.translate("DebugAt", u"\u53d6\u6d88", None))
    # retranslateUi

