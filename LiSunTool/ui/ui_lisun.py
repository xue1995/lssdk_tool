# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'lisunCXUyZW.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(970, 714)
        MainWindow.setMinimumSize(QSize(672, 536))
        self.action_2 = QAction(MainWindow)
        self.action_2.setObjectName(u"action_2")
        self.action_2.setCheckable(True)
        self.action_2.setChecked(True)
        self.action_2.setMenuRole(QAction.AboutQtRole)
        self.action_3 = QAction(MainWindow)
        self.action_3.setObjectName(u"action_3")
        self.action_3.setCheckable(True)
        self.menu_helpDirection = QAction(MainWindow)
        self.menu_helpDirection.setObjectName(u"menu_helpDirection")
        self.menu_helpAbout = QAction(MainWindow)
        self.menu_helpAbout.setObjectName(u"menu_helpAbout")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout_7 = QGridLayout(self.centralwidget)
        self.gridLayout_7.setSpacing(6)
        self.gridLayout_7.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_8 = QGridLayout()
        self.gridLayout_8.setSpacing(6)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.cmb_serialNum = QComboBox(self.centralwidget)
        self.cmb_serialNum.addItem("")
        self.cmb_serialNum.setObjectName(u"cmb_serialNum")
        self.cmb_serialNum.setMinimumSize(QSize(0, 30))
        self.cmb_serialNum.setMaximumSize(QSize(180, 16777215))

        self.gridLayout_8.addWidget(self.cmb_serialNum, 0, 1, 1, 1)

        self.lbl_serialNum = QLabel(self.centralwidget)
        self.lbl_serialNum.setObjectName(u"lbl_serialNum")
        self.lbl_serialNum.setMinimumSize(QSize(85, 30))
        self.lbl_serialNum.setMaximumSize(QSize(85, 16777215))

        self.gridLayout_8.addWidget(self.lbl_serialNum, 0, 0, 1, 1)

        self.btn_switchSerial = QPushButton(self.centralwidget)
        self.btn_switchSerial.setObjectName(u"btn_switchSerial")
        self.btn_switchSerial.setMinimumSize(QSize(0, 30))
        self.btn_switchSerial.setMaximumSize(QSize(16777215, 200))

        self.gridLayout_8.addWidget(self.btn_switchSerial, 0, 3, 1, 1)

        self.btn_scanSerial = QPushButton(self.centralwidget)
        self.btn_scanSerial.setObjectName(u"btn_scanSerial")
        self.btn_scanSerial.setMinimumSize(QSize(0, 30))

        self.gridLayout_8.addWidget(self.btn_scanSerial, 0, 2, 1, 1)


        self.gridLayout_7.addLayout(self.gridLayout_8, 0, 0, 1, 1)

        self.tab_func = QTabWidget(self.centralwidget)
        self.tab_func.setObjectName(u"tab_func")
        self.user = QWidget()
        self.user.setObjectName(u"user")
        self.gridLayout_45 = QGridLayout(self.user)
        self.gridLayout_45.setSpacing(6)
        self.gridLayout_45.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_45.setObjectName(u"gridLayout_45")
        self.gridLayout_44 = QGridLayout()
        self.gridLayout_44.setSpacing(6)
        self.gridLayout_44.setObjectName(u"gridLayout_44")
        self.grp_upDfu = QGroupBox(self.user)
        self.grp_upDfu.setObjectName(u"grp_upDfu")
        self.grp_upDfu.setMinimumSize(QSize(391, 105))
        self.gridLayout_6 = QGridLayout(self.grp_upDfu)
        self.gridLayout_6.setSpacing(6)
        self.gridLayout_6.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setSpacing(6)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setSpacing(6)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.lbl_upFilePath = QLabel(self.grp_upDfu)
        self.lbl_upFilePath.setObjectName(u"lbl_upFilePath")
        self.lbl_upFilePath.setMinimumSize(QSize(85, 30))
        self.lbl_upFilePath.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_2.addWidget(self.lbl_upFilePath, 0, 0, 1, 1)

        self.le_upFilePath = QLineEdit(self.grp_upDfu)
        self.le_upFilePath.setObjectName(u"le_upFilePath")
        self.le_upFilePath.setMinimumSize(QSize(0, 30))

        self.gridLayout_2.addWidget(self.le_upFilePath, 0, 1, 1, 1)


        self.gridLayout_3.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.brn_upFilePath = QPushButton(self.grp_upDfu)
        self.brn_upFilePath.setObjectName(u"brn_upFilePath")
        self.brn_upFilePath.setMinimumSize(QSize(100, 30))

        self.gridLayout_3.addWidget(self.brn_upFilePath, 0, 1, 1, 1)


        self.gridLayout_6.addLayout(self.gridLayout_3, 0, 0, 1, 1)

        self.gridLayout_12 = QGridLayout()
        self.gridLayout_12.setSpacing(6)
        self.gridLayout_12.setObjectName(u"gridLayout_12")
        self.prg_upBar = QProgressBar(self.grp_upDfu)
        self.prg_upBar.setObjectName(u"prg_upBar")
        self.prg_upBar.setMinimumSize(QSize(0, 30))
        self.prg_upBar.setLayoutDirection(Qt.LeftToRight)
        self.prg_upBar.setValue(0)

        self.gridLayout_12.addWidget(self.prg_upBar, 0, 0, 1, 1)

        self.brn_upStartUpgrade = QPushButton(self.grp_upDfu)
        self.brn_upStartUpgrade.setObjectName(u"brn_upStartUpgrade")
        self.brn_upStartUpgrade.setMinimumSize(QSize(100, 30))

        self.gridLayout_12.addWidget(self.brn_upStartUpgrade, 0, 1, 1, 1)


        self.gridLayout_6.addLayout(self.gridLayout_12, 1, 0, 1, 1)


        self.gridLayout_44.addWidget(self.grp_upDfu, 1, 0, 1, 1)

        self.gridLayout_43 = QGridLayout()
        self.gridLayout_43.setSpacing(6)
        self.gridLayout_43.setObjectName(u"gridLayout_43")
        self.gridLayout_9 = QGridLayout()
        self.gridLayout_9.setSpacing(6)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setSpacing(6)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout = QGridLayout()
        self.gridLayout.setSpacing(6)
        self.gridLayout.setObjectName(u"gridLayout")
        self.twd_otherType = QTreeWidget(self.user)
        QTreeWidgetItem(self.twd_otherType)
        QTreeWidgetItem(self.twd_otherType)
        QTreeWidgetItem(self.twd_otherType)
        self.twd_otherType.setObjectName(u"twd_otherType")
        self.twd_otherType.setMinimumSize(QSize(0, 120))
        self.twd_otherType.setMaximumSize(QSize(16777215, 120))

        self.gridLayout.addWidget(self.twd_otherType, 1, 0, 1, 1)

        self.twd_lightType = QTreeWidget(self.user)
        __qtreewidgetitem = QTreeWidgetItem(self.twd_lightType)
        __qtreewidgetitem1 = QTreeWidgetItem(__qtreewidgetitem)
        __qtreewidgetitem1.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEditable|Qt.ItemIsDragEnabled|Qt.ItemIsDropEnabled|Qt.ItemIsUserCheckable|Qt.ItemIsEnabled);
        __qtreewidgetitem2 = QTreeWidgetItem(__qtreewidgetitem)
        __qtreewidgetitem2.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEditable|Qt.ItemIsDragEnabled|Qt.ItemIsDropEnabled|Qt.ItemIsUserCheckable|Qt.ItemIsEnabled);
        __qtreewidgetitem3 = QTreeWidgetItem(__qtreewidgetitem)
        __qtreewidgetitem3.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEditable|Qt.ItemIsDragEnabled|Qt.ItemIsDropEnabled|Qt.ItemIsUserCheckable|Qt.ItemIsEnabled);
        __qtreewidgetitem4 = QTreeWidgetItem(self.twd_lightType)
        __qtreewidgetitem5 = QTreeWidgetItem(__qtreewidgetitem4)
        __qtreewidgetitem5.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEditable|Qt.ItemIsDragEnabled|Qt.ItemIsDropEnabled|Qt.ItemIsUserCheckable|Qt.ItemIsEnabled);
        __qtreewidgetitem6 = QTreeWidgetItem(__qtreewidgetitem4)
        __qtreewidgetitem6.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEditable|Qt.ItemIsDragEnabled|Qt.ItemIsDropEnabled|Qt.ItemIsUserCheckable|Qt.ItemIsEnabled);
        __qtreewidgetitem7 = QTreeWidgetItem(__qtreewidgetitem4)
        __qtreewidgetitem7.setFlags(Qt.ItemIsSelectable|Qt.ItemIsEditable|Qt.ItemIsDragEnabled|Qt.ItemIsDropEnabled|Qt.ItemIsUserCheckable|Qt.ItemIsEnabled);
        self.twd_lightType.setObjectName(u"twd_lightType")
        self.twd_lightType.setMinimumSize(QSize(0, 0))
        self.twd_lightType.setBaseSize(QSize(0, 50))
        self.twd_lightType.setStyleSheet(u"#treeWidget::item{height:100px;}")
        self.twd_lightType.setFrameShape(QFrame.StyledPanel)
        self.twd_lightType.setFrameShadow(QFrame.Plain)
        self.twd_lightType.setLineWidth(1)
        self.twd_lightType.setMidLineWidth(0)
        self.twd_lightType.setAutoScrollMargin(20)
        self.twd_lightType.setIndentation(20)
        self.twd_lightType.header().setCascadingSectionResizes(True)
        self.twd_lightType.header().setMinimumSectionSize(50)
        self.twd_lightType.header().setDefaultSectionSize(100)
        self.twd_lightType.header().setHighlightSections(True)

        self.gridLayout.addWidget(self.twd_lightType, 0, 0, 1, 1)


        self.gridLayout_4.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.lbl_lampCfgHelp = QLabel(self.user)
        self.lbl_lampCfgHelp.setObjectName(u"lbl_lampCfgHelp")

        self.gridLayout_4.addWidget(self.lbl_lampCfgHelp, 1, 0, 1, 1)


        self.gridLayout_9.addLayout(self.gridLayout_4, 0, 0, 1, 1)

        self.gridLayout_5 = QGridLayout()
        self.gridLayout_5.setSpacing(6)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.btn_deleteGear = QPushButton(self.user)
        self.btn_deleteGear.setObjectName(u"btn_deleteGear")
        self.btn_deleteGear.setMinimumSize(QSize(100, 30))

        self.gridLayout_5.addWidget(self.btn_deleteGear, 3, 0, 1, 1)

        self.btn_addMode = QPushButton(self.user)
        self.btn_addMode.setObjectName(u"btn_addMode")
        self.btn_addMode.setMinimumSize(QSize(110, 30))

        self.gridLayout_5.addWidget(self.btn_addMode, 0, 0, 1, 1)

        self.btn_addGear = QPushButton(self.user)
        self.btn_addGear.setObjectName(u"btn_addGear")
        self.btn_addGear.setMinimumSize(QSize(100, 30))

        self.gridLayout_5.addWidget(self.btn_addGear, 2, 0, 1, 1)

        self.btn_deleteMode = QPushButton(self.user)
        self.btn_deleteMode.setObjectName(u"btn_deleteMode")
        self.btn_deleteMode.setMinimumSize(QSize(100, 30))

        self.gridLayout_5.addWidget(self.btn_deleteMode, 1, 0, 1, 1)

        self.btn_writeConfig = QPushButton(self.user)
        self.btn_writeConfig.setObjectName(u"btn_writeConfig")
        self.btn_writeConfig.setMinimumSize(QSize(100, 30))

        self.gridLayout_5.addWidget(self.btn_writeConfig, 5, 0, 1, 1)

        self.btn_readConfig = QPushButton(self.user)
        self.btn_readConfig.setObjectName(u"btn_readConfig")
        self.btn_readConfig.setMinimumSize(QSize(100, 30))

        self.gridLayout_5.addWidget(self.btn_readConfig, 4, 0, 1, 1)

        self.btn_refreshLampInfo = QPushButton(self.user)
        self.btn_refreshLampInfo.setObjectName(u"btn_refreshLampInfo")
        self.btn_refreshLampInfo.setMinimumSize(QSize(100, 30))

        self.gridLayout_5.addWidget(self.btn_refreshLampInfo, 6, 0, 1, 1)


        self.gridLayout_9.addLayout(self.gridLayout_5, 0, 1, 1, 1)


        self.gridLayout_43.addLayout(self.gridLayout_9, 0, 0, 1, 1)

        self.grp_lampInfo = QGroupBox(self.user)
        self.grp_lampInfo.setObjectName(u"grp_lampInfo")
        self.grp_lampInfo.setMinimumSize(QSize(300, 0))
        self.grp_lampInfo.setMaximumSize(QSize(500, 16777215))
        self.gridLayout_53 = QGridLayout(self.grp_lampInfo)
        self.gridLayout_53.setSpacing(6)
        self.gridLayout_53.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_53.setObjectName(u"gridLayout_53")
        self.gridLayout_29 = QGridLayout()
        self.gridLayout_29.setSpacing(6)
        self.gridLayout_29.setObjectName(u"gridLayout_29")
        self.lbl_lampInfo1 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1.setObjectName(u"lbl_lampInfo1")
        self.lbl_lampInfo1.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_29.addWidget(self.lbl_lampInfo1, 0, 0, 1, 1)

        self.lbl_lampInfoProductName = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoProductName.setObjectName(u"lbl_lampInfoProductName")
        self.lbl_lampInfoProductName.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoProductName.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_29.addWidget(self.lbl_lampInfoProductName, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_29, 0, 0, 1, 1)

        self.gridLayout_30 = QGridLayout()
        self.gridLayout_30.setSpacing(6)
        self.gridLayout_30.setObjectName(u"gridLayout_30")
        self.lbl_lampInfo1_2 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_2.setObjectName(u"lbl_lampInfo1_2")
        self.lbl_lampInfo1_2.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_2.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_30.addWidget(self.lbl_lampInfo1_2, 0, 0, 1, 1)

        self.lbl_lampInfoFirmwareVer = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoFirmwareVer.setObjectName(u"lbl_lampInfoFirmwareVer")
        self.lbl_lampInfoFirmwareVer.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoFirmwareVer.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_30.addWidget(self.lbl_lampInfoFirmwareVer, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_30, 1, 0, 1, 1)

        self.gridLayout_31 = QGridLayout()
        self.gridLayout_31.setSpacing(6)
        self.gridLayout_31.setObjectName(u"gridLayout_31")
        self.lbl_lampInfo1_3 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_3.setObjectName(u"lbl_lampInfo1_3")
        self.lbl_lampInfo1_3.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_3.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_31.addWidget(self.lbl_lampInfo1_3, 0, 0, 1, 1)

        self.lbl_lampInfoFirmwareDate = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoFirmwareDate.setObjectName(u"lbl_lampInfoFirmwareDate")
        self.lbl_lampInfoFirmwareDate.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoFirmwareDate.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_31.addWidget(self.lbl_lampInfoFirmwareDate, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_31, 2, 0, 1, 1)

        self.gridLayout_32 = QGridLayout()
        self.gridLayout_32.setSpacing(6)
        self.gridLayout_32.setObjectName(u"gridLayout_32")
        self.lbl_lampInfo1_4 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_4.setObjectName(u"lbl_lampInfo1_4")
        self.lbl_lampInfo1_4.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_4.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_32.addWidget(self.lbl_lampInfo1_4, 0, 0, 1, 1)

        self.lbl_lampInfoMFR = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoMFR.setObjectName(u"lbl_lampInfoMFR")
        self.lbl_lampInfoMFR.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoMFR.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_32.addWidget(self.lbl_lampInfoMFR, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_32, 3, 0, 1, 1)

        self.gridLayout_33 = QGridLayout()
        self.gridLayout_33.setSpacing(6)
        self.gridLayout_33.setObjectName(u"gridLayout_33")
        self.lbl_lampInfo1_5 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_5.setObjectName(u"lbl_lampInfo1_5")
        self.lbl_lampInfo1_5.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_5.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_33.addWidget(self.lbl_lampInfo1_5, 0, 0, 1, 1)

        self.lbl_lampInfoState = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoState.setObjectName(u"lbl_lampInfoState")
        self.lbl_lampInfoState.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoState.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_33.addWidget(self.lbl_lampInfoState, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_33, 4, 0, 1, 1)

        self.gridLayout_34 = QGridLayout()
        self.gridLayout_34.setSpacing(6)
        self.gridLayout_34.setObjectName(u"gridLayout_34")
        self.lbl_lampInfo1_6 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_6.setObjectName(u"lbl_lampInfo1_6")
        self.lbl_lampInfo1_6.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_6.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_34.addWidget(self.lbl_lampInfo1_6, 0, 0, 1, 1)

        self.lbl_lampInfoMode = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoMode.setObjectName(u"lbl_lampInfoMode")
        self.lbl_lampInfoMode.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoMode.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_34.addWidget(self.lbl_lampInfoMode, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_34, 5, 0, 1, 1)

        self.gridLayout_35 = QGridLayout()
        self.gridLayout_35.setSpacing(6)
        self.gridLayout_35.setObjectName(u"gridLayout_35")
        self.lbl_lampInfo1_7 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_7.setObjectName(u"lbl_lampInfo1_7")
        self.lbl_lampInfo1_7.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_7.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_35.addWidget(self.lbl_lampInfo1_7, 0, 0, 1, 1)

        self.lbl_lampInfoGrade = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoGrade.setObjectName(u"lbl_lampInfoGrade")
        self.lbl_lampInfoGrade.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoGrade.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_35.addWidget(self.lbl_lampInfoGrade, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_35, 6, 0, 1, 1)

        self.gridLayout_36 = QGridLayout()
        self.gridLayout_36.setSpacing(6)
        self.gridLayout_36.setObjectName(u"gridLayout_36")
        self.lbl_lampInfo1_8 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_8.setObjectName(u"lbl_lampInfo1_8")
        self.lbl_lampInfo1_8.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_8.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_36.addWidget(self.lbl_lampInfo1_8, 0, 0, 1, 1)

        self.lbl_lampInfoLock = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoLock.setObjectName(u"lbl_lampInfoLock")
        self.lbl_lampInfoLock.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoLock.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_36.addWidget(self.lbl_lampInfoLock, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_36, 7, 0, 1, 1)

        self.gridLayout_37 = QGridLayout()
        self.gridLayout_37.setSpacing(6)
        self.gridLayout_37.setObjectName(u"gridLayout_37")
        self.lbl_lampInfo1_9 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_9.setObjectName(u"lbl_lampInfo1_9")
        self.lbl_lampInfo1_9.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_9.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_37.addWidget(self.lbl_lampInfo1_9, 0, 0, 1, 1)

        self.lbl_lampInfoDayNight = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoDayNight.setObjectName(u"lbl_lampInfoDayNight")
        self.lbl_lampInfoDayNight.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoDayNight.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_37.addWidget(self.lbl_lampInfoDayNight, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_37, 8, 0, 1, 1)

        self.gridLayout_38 = QGridLayout()
        self.gridLayout_38.setSpacing(6)
        self.gridLayout_38.setObjectName(u"gridLayout_38")
        self.lbl_lampInfo1_10 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_10.setObjectName(u"lbl_lampInfo1_10")
        self.lbl_lampInfo1_10.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_10.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_38.addWidget(self.lbl_lampInfo1_10, 0, 0, 1, 1)

        self.lbl_lampInfoBatSoc = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoBatSoc.setObjectName(u"lbl_lampInfoBatSoc")
        self.lbl_lampInfoBatSoc.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoBatSoc.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_38.addWidget(self.lbl_lampInfoBatSoc, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_38, 9, 0, 1, 1)

        self.gridLayout_39 = QGridLayout()
        self.gridLayout_39.setSpacing(6)
        self.gridLayout_39.setObjectName(u"gridLayout_39")
        self.lbl_lampInfo1_11 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_11.setObjectName(u"lbl_lampInfo1_11")
        self.lbl_lampInfo1_11.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_11.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_39.addWidget(self.lbl_lampInfo1_11, 0, 0, 1, 1)

        self.lbl_lampInfoBatVol = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoBatVol.setObjectName(u"lbl_lampInfoBatVol")
        self.lbl_lampInfoBatVol.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoBatVol.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_39.addWidget(self.lbl_lampInfoBatVol, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_39, 10, 0, 1, 1)

        self.gridLayout_47 = QGridLayout()
        self.gridLayout_47.setSpacing(6)
        self.gridLayout_47.setObjectName(u"gridLayout_47")
        self.lbl_lampInfo1_12 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_12.setObjectName(u"lbl_lampInfo1_12")
        self.lbl_lampInfo1_12.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_12.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_47.addWidget(self.lbl_lampInfo1_12, 0, 0, 1, 1)

        self.lbl_lampInfoBatFullVol = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoBatFullVol.setObjectName(u"lbl_lampInfoBatFullVol")
        self.lbl_lampInfoBatFullVol.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoBatFullVol.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_47.addWidget(self.lbl_lampInfoBatFullVol, 0, 1, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_47, 11, 0, 1, 1)

        self.gridLayout_49 = QGridLayout()
        self.gridLayout_49.setSpacing(6)
        self.gridLayout_49.setObjectName(u"gridLayout_49")
        self.lbl_lampInfoBatState = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoBatState.setObjectName(u"lbl_lampInfoBatState")
        self.lbl_lampInfoBatState.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoBatState.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_49.addWidget(self.lbl_lampInfoBatState, 0, 1, 1, 1)

        self.lbl_lampInfo1_14 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_14.setObjectName(u"lbl_lampInfo1_14")
        self.lbl_lampInfo1_14.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_14.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_49.addWidget(self.lbl_lampInfo1_14, 0, 0, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_49, 12, 0, 1, 1)

        self.gridLayout_51 = QGridLayout()
        self.gridLayout_51.setSpacing(6)
        self.gridLayout_51.setObjectName(u"gridLayout_51")
        self.lbl_lampInfoLuminPct = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoLuminPct.setObjectName(u"lbl_lampInfoLuminPct")
        self.lbl_lampInfoLuminPct.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoLuminPct.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_51.addWidget(self.lbl_lampInfoLuminPct, 0, 1, 1, 1)

        self.lbl_lampInfo1_16 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_16.setObjectName(u"lbl_lampInfo1_16")
        self.lbl_lampInfo1_16.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_16.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_51.addWidget(self.lbl_lampInfo1_16, 0, 0, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_51, 13, 0, 1, 1)

        self.gridLayout_52 = QGridLayout()
        self.gridLayout_52.setSpacing(6)
        self.gridLayout_52.setObjectName(u"gridLayout_52")
        self.lbl_lampInfoTemp = QLabel(self.grp_lampInfo)
        self.lbl_lampInfoTemp.setObjectName(u"lbl_lampInfoTemp")
        self.lbl_lampInfoTemp.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfoTemp.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_52.addWidget(self.lbl_lampInfoTemp, 0, 1, 1, 1)

        self.lbl_lampInfo1_17 = QLabel(self.grp_lampInfo)
        self.lbl_lampInfo1_17.setObjectName(u"lbl_lampInfo1_17")
        self.lbl_lampInfo1_17.setMinimumSize(QSize(85, 25))
        self.lbl_lampInfo1_17.setMaximumSize(QSize(120, 16777215))

        self.gridLayout_52.addWidget(self.lbl_lampInfo1_17, 0, 0, 1, 1)


        self.gridLayout_53.addLayout(self.gridLayout_52, 14, 0, 1, 1)


        self.gridLayout_43.addWidget(self.grp_lampInfo, 0, 1, 1, 1)


        self.gridLayout_44.addLayout(self.gridLayout_43, 0, 0, 1, 1)


        self.gridLayout_45.addLayout(self.gridLayout_44, 0, 0, 1, 1)

        self.tab_func.addTab(self.user, "")
        self.debug = QWidget()
        self.debug.setObjectName(u"debug")
        self.gridLayout_42 = QGridLayout(self.debug)
        self.gridLayout_42.setSpacing(6)
        self.gridLayout_42.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_42.setObjectName(u"gridLayout_42")
        self.pte_debugReceive = QPlainTextEdit(self.debug)
        self.pte_debugReceive.setObjectName(u"pte_debugReceive")
        self.pte_debugReceive.setMinimumSize(QSize(370, 451))
        self.pte_debugReceive.setAcceptDrops(False)
        self.pte_debugReceive.setUndoRedoEnabled(False)
        self.pte_debugReceive.setLineWrapMode(QPlainTextEdit.NoWrap)
        self.pte_debugReceive.setTextInteractionFlags(Qt.TextSelectableByMouse)

        self.gridLayout_42.addWidget(self.pte_debugReceive, 0, 0, 1, 1)

        self.lvw_debugAT = QListWidget(self.debug)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        QListWidgetItem(self.lvw_debugAT)
        self.lvw_debugAT.setObjectName(u"lvw_debugAT")
        self.lvw_debugAT.setMinimumSize(QSize(130, 0))
        self.lvw_debugAT.setMaximumSize(QSize(130, 16777215))
        self.lvw_debugAT.setContextMenuPolicy(Qt.CustomContextMenu)
        self.lvw_debugAT.setToolTipDuration(-1)

        self.gridLayout_42.addWidget(self.lvw_debugAT, 0, 1, 2, 1)

        self.tbw_loopDebugAT = QTableWidget(self.debug)
        if (self.tbw_loopDebugAT.columnCount() < 2):
            self.tbw_loopDebugAT.setColumnCount(2)
        __qtablewidgetitem = QTableWidgetItem()
        self.tbw_loopDebugAT.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tbw_loopDebugAT.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        if (self.tbw_loopDebugAT.rowCount() < 6):
            self.tbw_loopDebugAT.setRowCount(6)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tbw_loopDebugAT.setVerticalHeaderItem(0, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tbw_loopDebugAT.setVerticalHeaderItem(1, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.tbw_loopDebugAT.setVerticalHeaderItem(2, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.tbw_loopDebugAT.setVerticalHeaderItem(3, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        self.tbw_loopDebugAT.setVerticalHeaderItem(4, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        self.tbw_loopDebugAT.setVerticalHeaderItem(5, __qtablewidgetitem7)
        __qtablewidgetitem8 = QTableWidgetItem()
        __qtablewidgetitem8.setCheckState(Qt.Checked);
        self.tbw_loopDebugAT.setItem(0, 0, __qtablewidgetitem8)
        __qtablewidgetitem9 = QTableWidgetItem()
        self.tbw_loopDebugAT.setItem(0, 1, __qtablewidgetitem9)
        __qtablewidgetitem10 = QTableWidgetItem()
        __qtablewidgetitem10.setCheckState(Qt.Checked);
        self.tbw_loopDebugAT.setItem(1, 0, __qtablewidgetitem10)
        __qtablewidgetitem11 = QTableWidgetItem()
        self.tbw_loopDebugAT.setItem(1, 1, __qtablewidgetitem11)
        __qtablewidgetitem12 = QTableWidgetItem()
        __qtablewidgetitem12.setCheckState(Qt.Checked);
        self.tbw_loopDebugAT.setItem(2, 0, __qtablewidgetitem12)
        __qtablewidgetitem13 = QTableWidgetItem()
        self.tbw_loopDebugAT.setItem(2, 1, __qtablewidgetitem13)
        __qtablewidgetitem14 = QTableWidgetItem()
        __qtablewidgetitem14.setCheckState(Qt.Checked);
        self.tbw_loopDebugAT.setItem(3, 0, __qtablewidgetitem14)
        __qtablewidgetitem15 = QTableWidgetItem()
        self.tbw_loopDebugAT.setItem(3, 1, __qtablewidgetitem15)
        __qtablewidgetitem16 = QTableWidgetItem()
        __qtablewidgetitem16.setCheckState(Qt.Checked);
        self.tbw_loopDebugAT.setItem(4, 0, __qtablewidgetitem16)
        __qtablewidgetitem17 = QTableWidgetItem()
        self.tbw_loopDebugAT.setItem(4, 1, __qtablewidgetitem17)
        __qtablewidgetitem18 = QTableWidgetItem()
        __qtablewidgetitem18.setCheckState(Qt.Checked);
        self.tbw_loopDebugAT.setItem(5, 0, __qtablewidgetitem18)
        __qtablewidgetitem19 = QTableWidgetItem()
        self.tbw_loopDebugAT.setItem(5, 1, __qtablewidgetitem19)
        self.tbw_loopDebugAT.setObjectName(u"tbw_loopDebugAT")
        self.tbw_loopDebugAT.setMinimumSize(QSize(300, 551))
        self.tbw_loopDebugAT.setMaximumSize(QSize(320, 16777215))
        self.tbw_loopDebugAT.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tbw_loopDebugAT.setFrameShadow(QFrame.Sunken)
        self.tbw_loopDebugAT.setLineWidth(500)
        self.tbw_loopDebugAT.setMidLineWidth(500)
        self.tbw_loopDebugAT.horizontalHeader().setMinimumSectionSize(20)
        self.tbw_loopDebugAT.horizontalHeader().setDefaultSectionSize(135)
        self.tbw_loopDebugAT.horizontalHeader().setHighlightSections(True)
        self.tbw_loopDebugAT.verticalHeader().setMinimumSectionSize(27)
        self.tbw_loopDebugAT.verticalHeader().setDefaultSectionSize(27)

        self.gridLayout_42.addWidget(self.tbw_loopDebugAT, 0, 2, 2, 1)

        self.gridLayout_41 = QGridLayout()
        self.gridLayout_41.setSpacing(6)
        self.gridLayout_41.setObjectName(u"gridLayout_41")
        self.gridLayout_101 = QGridLayout()
        self.gridLayout_101.setSpacing(6)
        self.gridLayout_101.setObjectName(u"gridLayout_101")
        self.verticalSpacer_7 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_101.addItem(self.verticalSpacer_7, 5, 0, 1, 1)

        self.lbl_loopHelp = QLabel(self.debug)
        self.lbl_loopHelp.setObjectName(u"lbl_loopHelp")
        self.lbl_loopHelp.setMaximumSize(QSize(100, 16777215))

        self.gridLayout_101.addWidget(self.lbl_loopHelp, 2, 0, 1, 1)

        self.lbl_residueDegree_3 = QLabel(self.debug)
        self.lbl_residueDegree_3.setObjectName(u"lbl_residueDegree_3")
        self.lbl_residueDegree_3.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_101.addWidget(self.lbl_residueDegree_3, 7, 0, 1, 1)

        self.le_loopDebugSendNum = QLineEdit(self.debug)
        self.le_loopDebugSendNum.setObjectName(u"le_loopDebugSendNum")
        self.le_loopDebugSendNum.setMaximumSize(QSize(100, 16777215))
        self.le_loopDebugSendNum.setAlignment(Qt.AlignCenter)

        self.gridLayout_101.addWidget(self.le_loopDebugSendNum, 1, 0, 1, 1)

        self.lbl_loopDebugSendNum_5 = QLabel(self.debug)
        self.lbl_loopDebugSendNum_5.setObjectName(u"lbl_loopDebugSendNum_5")
        self.lbl_loopDebugSendNum_5.setMinimumSize(QSize(100, 0))
        self.lbl_loopDebugSendNum_5.setMaximumSize(QSize(110, 16777215))

        self.gridLayout_101.addWidget(self.lbl_loopDebugSendNum_5, 0, 0, 1, 1)

        self.lbl_residueDegree_2 = QLabel(self.debug)
        self.lbl_residueDegree_2.setObjectName(u"lbl_residueDegree_2")
        self.lbl_residueDegree_2.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_101.addWidget(self.lbl_residueDegree_2, 6, 0, 1, 1)

        self.lbl_residueDegree = QLabel(self.debug)
        self.lbl_residueDegree.setObjectName(u"lbl_residueDegree")
        self.lbl_residueDegree.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_101.addWidget(self.lbl_residueDegree, 8, 0, 1, 1)

        self.verticalSpacer_23 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_101.addItem(self.verticalSpacer_23, 3, 0, 1, 1)

        self.verticalSpacer_8 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_101.addItem(self.verticalSpacer_8, 4, 0, 1, 1)


        self.gridLayout_41.addLayout(self.gridLayout_101, 0, 0, 1, 1)

        self.gridLayout_40 = QGridLayout()
        self.gridLayout_40.setSpacing(6)
        self.gridLayout_40.setObjectName(u"gridLayout_40")
        self.gridLayout_24 = QGridLayout()
        self.gridLayout_24.setSpacing(6)
        self.gridLayout_24.setObjectName(u"gridLayout_24")
        self.gridLayout_11 = QGridLayout()
        self.gridLayout_11.setSpacing(6)
        self.gridLayout_11.setObjectName(u"gridLayout_11")
        self.btn_debugCleanSendText = QPushButton(self.debug)
        self.btn_debugCleanSendText.setObjectName(u"btn_debugCleanSendText")
        self.btn_debugCleanSendText.setMinimumSize(QSize(100, 30))
        self.btn_debugCleanSendText.setMaximumSize(QSize(100, 16777215))

        self.gridLayout_11.addWidget(self.btn_debugCleanSendText, 1, 0, 1, 1)

        self.btn_debugSend = QPushButton(self.debug)
        self.btn_debugSend.setObjectName(u"btn_debugSend")
        self.btn_debugSend.setMinimumSize(QSize(100, 30))
        self.btn_debugSend.setMaximumSize(QSize(100, 16777215))
        font = QFont()
        font.setFamily(u"\u5b8b\u4f53")
        self.btn_debugSend.setFont(font)

        self.gridLayout_11.addWidget(self.btn_debugSend, 0, 0, 1, 1)


        self.gridLayout_24.addLayout(self.gridLayout_11, 2, 0, 1, 1)

        self.gridLayout_10 = QGridLayout()
        self.gridLayout_10.setSpacing(6)
        self.gridLayout_10.setObjectName(u"gridLayout_10")
        self.btn_debugCleanReceiveText = QPushButton(self.debug)
        self.btn_debugCleanReceiveText.setObjectName(u"btn_debugCleanReceiveText")
        self.btn_debugCleanReceiveText.setMinimumSize(QSize(100, 30))
        self.btn_debugCleanReceiveText.setMaximumSize(QSize(100, 30))
        self.btn_debugCleanReceiveText.setFont(font)

        self.gridLayout_10.addWidget(self.btn_debugCleanReceiveText, 1, 0, 1, 1)

        self.btn_debugSaveReceiveText = QPushButton(self.debug)
        self.btn_debugSaveReceiveText.setObjectName(u"btn_debugSaveReceiveText")
        self.btn_debugSaveReceiveText.setMinimumSize(QSize(100, 30))
        self.btn_debugSaveReceiveText.setMaximumSize(QSize(100, 16777215))

        self.gridLayout_10.addWidget(self.btn_debugSaveReceiveText, 0, 0, 1, 1)


        self.gridLayout_24.addLayout(self.gridLayout_10, 0, 0, 1, 1)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_24.addItem(self.verticalSpacer_3, 1, 0, 1, 1)


        self.gridLayout_40.addLayout(self.gridLayout_24, 3, 0, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_40.addItem(self.verticalSpacer_2, 2, 0, 1, 1)

        self.gridLayout_21 = QGridLayout()
        self.gridLayout_21.setSpacing(6)
        self.gridLayout_21.setObjectName(u"gridLayout_21")
        self.btn_loopDebugStart = QPushButton(self.debug)
        self.btn_loopDebugStart.setObjectName(u"btn_loopDebugStart")
        self.btn_loopDebugStart.setMinimumSize(QSize(100, 30))
        self.btn_loopDebugStart.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_21.addWidget(self.btn_loopDebugStart, 2, 0, 1, 1)

        self.gridLayout_14 = QGridLayout()
        self.gridLayout_14.setSpacing(6)
        self.gridLayout_14.setObjectName(u"gridLayout_14")
        self.gridLayout_13 = QGridLayout()
        self.gridLayout_13.setSpacing(6)
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.btn_loopAddLine = QPushButton(self.debug)
        self.btn_loopAddLine.setObjectName(u"btn_loopAddLine")
        self.btn_loopAddLine.setMinimumSize(QSize(100, 30))
        self.btn_loopAddLine.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_13.addWidget(self.btn_loopAddLine, 0, 0, 1, 1)

        self.btn_loopDelLine = QPushButton(self.debug)
        self.btn_loopDelLine.setObjectName(u"btn_loopDelLine")
        self.btn_loopDelLine.setMinimumSize(QSize(100, 30))
        self.btn_loopDelLine.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_13.addWidget(self.btn_loopDelLine, 1, 0, 1, 1)


        self.gridLayout_14.addLayout(self.gridLayout_13, 2, 0, 1, 1)

        self.gridLayout_103 = QGridLayout()
        self.gridLayout_103.setSpacing(6)
        self.gridLayout_103.setObjectName(u"gridLayout_103")
        self.btn_loopDebugFull = QPushButton(self.debug)
        self.btn_loopDebugFull.setObjectName(u"btn_loopDebugFull")
        self.btn_loopDebugFull.setMinimumSize(QSize(100, 30))
        self.btn_loopDebugFull.setMaximumSize(QSize(100, 16777215))

        self.gridLayout_103.addWidget(self.btn_loopDebugFull, 0, 0, 1, 1)

        self.btn_loopDebugNop = QPushButton(self.debug)
        self.btn_loopDebugNop.setObjectName(u"btn_loopDebugNop")
        self.btn_loopDebugNop.setMinimumSize(QSize(100, 30))
        self.btn_loopDebugNop.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_103.addWidget(self.btn_loopDebugNop, 1, 0, 1, 1)


        self.gridLayout_14.addLayout(self.gridLayout_103, 0, 0, 1, 1)

        self.verticalSpacer_5 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_14.addItem(self.verticalSpacer_5, 1, 0, 1, 1)


        self.gridLayout_21.addLayout(self.gridLayout_14, 0, 0, 1, 1)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_21.addItem(self.verticalSpacer_4, 1, 0, 1, 1)


        self.gridLayout_40.addLayout(self.gridLayout_21, 1, 0, 1, 1)

        self.verticalSpacer_6 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_40.addItem(self.verticalSpacer_6, 0, 0, 1, 1)


        self.gridLayout_41.addLayout(self.gridLayout_40, 1, 0, 1, 1)


        self.gridLayout_42.addLayout(self.gridLayout_41, 0, 3, 2, 1)

        self.pte_debugSend = QPlainTextEdit(self.debug)
        self.pte_debugSend.setObjectName(u"pte_debugSend")
        self.pte_debugSend.setMinimumSize(QSize(300, 70))
        self.pte_debugSend.setMaximumSize(QSize(16777215, 70))
        self.pte_debugSend.setTextInteractionFlags(Qt.TextEditorInteraction)

        self.gridLayout_42.addWidget(self.pte_debugSend, 1, 0, 1, 1)

        self.tab_func.addTab(self.debug, "")
        self.graph = QWidget()
        self.graph.setObjectName(u"graph")
        self.gridLayout_16 = QGridLayout(self.graph)
        self.gridLayout_16.setSpacing(6)
        self.gridLayout_16.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_16.setObjectName(u"gridLayout_16")
        self.widget = QWidget(self.graph)
        self.widget.setObjectName(u"widget")
        self.widget.setLayoutDirection(Qt.LeftToRight)

        self.gridLayout_16.addWidget(self.widget, 0, 0, 1, 1)

        self.groupBox = QGroupBox(self.graph)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setMaximumSize(QSize(16777215, 80))
        self.gridLayout_19 = QGridLayout(self.groupBox)
        self.gridLayout_19.setSpacing(6)
        self.gridLayout_19.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_19.setObjectName(u"gridLayout_19")
        self.gridLayout_18 = QGridLayout()
        self.gridLayout_18.setSpacing(6)
        self.gridLayout_18.setObjectName(u"gridLayout_18")
        self.btn_ghStartRecord = QPushButton(self.groupBox)
        self.btn_ghStartRecord.setObjectName(u"btn_ghStartRecord")
        self.btn_ghStartRecord.setMinimumSize(QSize(0, 30))

        self.gridLayout_18.addWidget(self.btn_ghStartRecord, 0, 0, 1, 1)

        self.btn_ghClearData = QPushButton(self.groupBox)
        self.btn_ghClearData.setObjectName(u"btn_ghClearData")
        self.btn_ghClearData.setMinimumSize(QSize(0, 30))

        self.gridLayout_18.addWidget(self.btn_ghClearData, 0, 1, 1, 1)

        self.btn_ghSavaData = QPushButton(self.groupBox)
        self.btn_ghSavaData.setObjectName(u"btn_ghSavaData")
        self.btn_ghSavaData.setMinimumSize(QSize(0, 30))

        self.gridLayout_18.addWidget(self.btn_ghSavaData, 0, 2, 1, 1)


        self.gridLayout_19.addLayout(self.gridLayout_18, 0, 0, 1, 1)


        self.gridLayout_16.addWidget(self.groupBox, 1, 0, 1, 1)

        self.tab_func.addTab(self.graph, "")
        self.FactoryBurn = QWidget()
        self.FactoryBurn.setObjectName(u"FactoryBurn")
        self.gridLayout_28 = QGridLayout(self.FactoryBurn)
        self.gridLayout_28.setSpacing(6)
        self.gridLayout_28.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_28.setObjectName(u"gridLayout_28")
        self.grp_jlink = QGroupBox(self.FactoryBurn)
        self.grp_jlink.setObjectName(u"grp_jlink")
        self.grp_jlink.setMinimumSize(QSize(700, 140))
        self.grp_jlink.setMaximumSize(QSize(16777215, 150))
        self.gridLayout_23 = QGridLayout(self.grp_jlink)
        self.gridLayout_23.setSpacing(6)
        self.gridLayout_23.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_23.setObjectName(u"gridLayout_23")
        self.gridLayout_17 = QGridLayout()
        self.gridLayout_17.setSpacing(6)
        self.gridLayout_17.setObjectName(u"gridLayout_17")
        self.brn_jlinkFolderPath = QPushButton(self.grp_jlink)
        self.brn_jlinkFolderPath.setObjectName(u"brn_jlinkFolderPath")
        self.brn_jlinkFolderPath.setMinimumSize(QSize(100, 30))

        self.gridLayout_17.addWidget(self.brn_jlinkFolderPath, 0, 2, 1, 1)

        self.lbl_jlinkFilePath = QLabel(self.grp_jlink)
        self.lbl_jlinkFilePath.setObjectName(u"lbl_jlinkFilePath")
        self.lbl_jlinkFilePath.setMinimumSize(QSize(90, 30))
        self.lbl_jlinkFilePath.setMaximumSize(QSize(90, 16777215))

        self.gridLayout_17.addWidget(self.lbl_jlinkFilePath, 0, 0, 1, 1)

        self.le_jlinkFilePath = QLineEdit(self.grp_jlink)
        self.le_jlinkFilePath.setObjectName(u"le_jlinkFilePath")
        self.le_jlinkFilePath.setMinimumSize(QSize(0, 30))

        self.gridLayout_17.addWidget(self.le_jlinkFilePath, 0, 1, 1, 1)

        self.brn_jlinkFindPath = QPushButton(self.grp_jlink)
        self.brn_jlinkFindPath.setObjectName(u"brn_jlinkFindPath")
        self.brn_jlinkFindPath.setMinimumSize(QSize(100, 30))

        self.gridLayout_17.addWidget(self.brn_jlinkFindPath, 0, 3, 1, 1)


        self.gridLayout_23.addLayout(self.gridLayout_17, 0, 0, 1, 1)

        self.gridLayout_22 = QGridLayout()
        self.gridLayout_22.setSpacing(6)
        self.gridLayout_22.setObjectName(u"gridLayout_22")
        self.le_jlinkBurnAddr = QLineEdit(self.grp_jlink)
        self.le_jlinkBurnAddr.setObjectName(u"le_jlinkBurnAddr")
        self.le_jlinkBurnAddr.setMinimumSize(QSize(0, 30))
        self.le_jlinkBurnAddr.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_22.addWidget(self.le_jlinkBurnAddr, 0, 4, 1, 1)

        self.chk_jlinkChipLock = QCheckBox(self.grp_jlink)
        self.chk_jlinkChipLock.setObjectName(u"chk_jlinkChipLock")

        self.gridLayout_22.addWidget(self.chk_jlinkChipLock, 0, 5, 1, 1)

        self.cmb_jlinkChipType = QComboBox(self.grp_jlink)
        self.cmb_jlinkChipType.addItem("")
        self.cmb_jlinkChipType.addItem("")
        self.cmb_jlinkChipType.addItem("")
        self.cmb_jlinkChipType.addItem("")
        self.cmb_jlinkChipType.addItem("")
        self.cmb_jlinkChipType.addItem("")
        self.cmb_jlinkChipType.addItem("")
        self.cmb_jlinkChipType.setObjectName(u"cmb_jlinkChipType")
        self.cmb_jlinkChipType.setMinimumSize(QSize(0, 30))
        self.cmb_jlinkChipType.setMaximumSize(QSize(180, 16777215))

        self.gridLayout_22.addWidget(self.cmb_jlinkChipType, 0, 2, 1, 1)

        self.lbl_jlinkChipType = QLabel(self.grp_jlink)
        self.lbl_jlinkChipType.setObjectName(u"lbl_jlinkChipType")
        self.lbl_jlinkChipType.setMinimumSize(QSize(90, 30))
        self.lbl_jlinkChipType.setMaximumSize(QSize(90, 16777215))

        self.gridLayout_22.addWidget(self.lbl_jlinkChipType, 0, 1, 1, 1)

        self.brn_jlinkStartBurnFile = QPushButton(self.grp_jlink)
        self.brn_jlinkStartBurnFile.setObjectName(u"brn_jlinkStartBurnFile")
        self.brn_jlinkStartBurnFile.setMinimumSize(QSize(100, 30))

        self.gridLayout_22.addWidget(self.brn_jlinkStartBurnFile, 0, 6, 1, 1)

        self.lbl_jlinkBurnAddr = QLabel(self.grp_jlink)
        self.lbl_jlinkBurnAddr.setObjectName(u"lbl_jlinkBurnAddr")
        self.lbl_jlinkBurnAddr.setMinimumSize(QSize(85, 30))
        self.lbl_jlinkBurnAddr.setMaximumSize(QSize(85, 16777215))

        self.gridLayout_22.addWidget(self.lbl_jlinkBurnAddr, 0, 3, 1, 1)

        self.brn_jlinkChipUnLock = QPushButton(self.grp_jlink)
        self.brn_jlinkChipUnLock.setObjectName(u"brn_jlinkChipUnLock")
        self.brn_jlinkChipUnLock.setMinimumSize(QSize(100, 30))

        self.gridLayout_22.addWidget(self.brn_jlinkChipUnLock, 0, 7, 1, 1)


        self.gridLayout_23.addLayout(self.gridLayout_22, 2, 0, 1, 1)

        self.gridLayout_20 = QGridLayout()
        self.gridLayout_20.setSpacing(6)
        self.gridLayout_20.setObjectName(u"gridLayout_20")
        self.lbl_jlinkBurnFilePath = QLabel(self.grp_jlink)
        self.lbl_jlinkBurnFilePath.setObjectName(u"lbl_jlinkBurnFilePath")
        self.lbl_jlinkBurnFilePath.setMinimumSize(QSize(90, 30))
        self.lbl_jlinkBurnFilePath.setMaximumSize(QSize(90, 16777215))

        self.gridLayout_20.addWidget(self.lbl_jlinkBurnFilePath, 0, 0, 1, 1)

        self.brn_jlinkBurnFilePath = QPushButton(self.grp_jlink)
        self.brn_jlinkBurnFilePath.setObjectName(u"brn_jlinkBurnFilePath")
        self.brn_jlinkBurnFilePath.setMinimumSize(QSize(100, 30))

        self.gridLayout_20.addWidget(self.brn_jlinkBurnFilePath, 0, 2, 1, 1)

        self.le_jlinkBurnFilePath = QLineEdit(self.grp_jlink)
        self.le_jlinkBurnFilePath.setObjectName(u"le_jlinkBurnFilePath")
        self.le_jlinkBurnFilePath.setMinimumSize(QSize(0, 30))

        self.gridLayout_20.addWidget(self.le_jlinkBurnFilePath, 0, 1, 1, 1)


        self.gridLayout_23.addLayout(self.gridLayout_20, 1, 0, 1, 1)


        self.gridLayout_28.addWidget(self.grp_jlink, 0, 0, 1, 1)

        self.grp_stlink = QGroupBox(self.FactoryBurn)
        self.grp_stlink.setObjectName(u"grp_stlink")
        self.grp_stlink.setMinimumSize(QSize(700, 140))
        self.grp_stlink.setMaximumSize(QSize(16777215, 150))
        self.gridLayout_15 = QGridLayout(self.grp_stlink)
        self.gridLayout_15.setSpacing(6)
        self.gridLayout_15.setContentsMargins(9, 9, 9, 9)
        self.gridLayout_15.setObjectName(u"gridLayout_15")
        self.gridLayout_25 = QGridLayout()
        self.gridLayout_25.setSpacing(6)
        self.gridLayout_25.setObjectName(u"gridLayout_25")
        self.brn_stlinkFolderPath = QPushButton(self.grp_stlink)
        self.brn_stlinkFolderPath.setObjectName(u"brn_stlinkFolderPath")
        self.brn_stlinkFolderPath.setMinimumSize(QSize(100, 30))

        self.gridLayout_25.addWidget(self.brn_stlinkFolderPath, 0, 2, 1, 1)

        self.lbl_stlinkFilePath = QLabel(self.grp_stlink)
        self.lbl_stlinkFilePath.setObjectName(u"lbl_stlinkFilePath")
        self.lbl_stlinkFilePath.setMinimumSize(QSize(90, 30))
        self.lbl_stlinkFilePath.setMaximumSize(QSize(90, 16777215))

        self.gridLayout_25.addWidget(self.lbl_stlinkFilePath, 0, 0, 1, 1)

        self.le_stlinkFilePath = QLineEdit(self.grp_stlink)
        self.le_stlinkFilePath.setObjectName(u"le_stlinkFilePath")
        self.le_stlinkFilePath.setMinimumSize(QSize(0, 30))

        self.gridLayout_25.addWidget(self.le_stlinkFilePath, 0, 1, 1, 1)

        self.brn_stlinkFindPath = QPushButton(self.grp_stlink)
        self.brn_stlinkFindPath.setObjectName(u"brn_stlinkFindPath")
        self.brn_stlinkFindPath.setMinimumSize(QSize(100, 30))

        self.gridLayout_25.addWidget(self.brn_stlinkFindPath, 0, 3, 1, 1)


        self.gridLayout_15.addLayout(self.gridLayout_25, 0, 0, 1, 1)

        self.gridLayout_27 = QGridLayout()
        self.gridLayout_27.setSpacing(6)
        self.gridLayout_27.setObjectName(u"gridLayout_27")
        self.lbl_stlinkBurnFilePath = QLabel(self.grp_stlink)
        self.lbl_stlinkBurnFilePath.setObjectName(u"lbl_stlinkBurnFilePath")
        self.lbl_stlinkBurnFilePath.setMinimumSize(QSize(90, 30))
        self.lbl_stlinkBurnFilePath.setMaximumSize(QSize(90, 16777215))

        self.gridLayout_27.addWidget(self.lbl_stlinkBurnFilePath, 0, 0, 1, 1)

        self.brn_stlinkBurnFilePath = QPushButton(self.grp_stlink)
        self.brn_stlinkBurnFilePath.setObjectName(u"brn_stlinkBurnFilePath")
        self.brn_stlinkBurnFilePath.setMinimumSize(QSize(100, 30))

        self.gridLayout_27.addWidget(self.brn_stlinkBurnFilePath, 0, 2, 1, 1)

        self.le_stlinkBurnFilePath = QLineEdit(self.grp_stlink)
        self.le_stlinkBurnFilePath.setObjectName(u"le_stlinkBurnFilePath")
        self.le_stlinkBurnFilePath.setMinimumSize(QSize(0, 30))

        self.gridLayout_27.addWidget(self.le_stlinkBurnFilePath, 0, 1, 1, 1)


        self.gridLayout_15.addLayout(self.gridLayout_27, 1, 0, 1, 1)

        self.gridLayout_26 = QGridLayout()
        self.gridLayout_26.setSpacing(6)
        self.gridLayout_26.setObjectName(u"gridLayout_26")
        self.le_stlinkBurnAddr = QLineEdit(self.grp_stlink)
        self.le_stlinkBurnAddr.setObjectName(u"le_stlinkBurnAddr")
        self.le_stlinkBurnAddr.setMinimumSize(QSize(0, 30))
        self.le_stlinkBurnAddr.setMaximumSize(QSize(80, 16777215))

        self.gridLayout_26.addWidget(self.le_stlinkBurnAddr, 0, 4, 1, 1)

        self.brn_stlinkStartBurnFile = QPushButton(self.grp_stlink)
        self.brn_stlinkStartBurnFile.setObjectName(u"brn_stlinkStartBurnFile")
        self.brn_stlinkStartBurnFile.setMinimumSize(QSize(100, 30))

        self.gridLayout_26.addWidget(self.brn_stlinkStartBurnFile, 0, 7, 1, 1)

        self.lbl_stlinkChipType = QLabel(self.grp_stlink)
        self.lbl_stlinkChipType.setObjectName(u"lbl_stlinkChipType")
        self.lbl_stlinkChipType.setMinimumSize(QSize(90, 30))
        self.lbl_stlinkChipType.setMaximumSize(QSize(90, 16777215))

        self.gridLayout_26.addWidget(self.lbl_stlinkChipType, 0, 1, 1, 1)

        self.cmb_stlinkChipType = QComboBox(self.grp_stlink)
        self.cmb_stlinkChipType.addItem("")
        self.cmb_stlinkChipType.addItem("")
        self.cmb_stlinkChipType.addItem("")
        self.cmb_stlinkChipType.addItem("")
        self.cmb_stlinkChipType.addItem("")
        self.cmb_stlinkChipType.addItem("")
        self.cmb_stlinkChipType.addItem("")
        self.cmb_stlinkChipType.setObjectName(u"cmb_stlinkChipType")
        self.cmb_stlinkChipType.setMinimumSize(QSize(0, 30))
        self.cmb_stlinkChipType.setMaximumSize(QSize(180, 16777215))

        self.gridLayout_26.addWidget(self.cmb_stlinkChipType, 0, 2, 1, 1)

        self.lbl_stlinkBurnAddr = QLabel(self.grp_stlink)
        self.lbl_stlinkBurnAddr.setObjectName(u"lbl_stlinkBurnAddr")
        self.lbl_stlinkBurnAddr.setMinimumSize(QSize(85, 30))
        self.lbl_stlinkBurnAddr.setMaximumSize(QSize(85, 16777215))

        self.gridLayout_26.addWidget(self.lbl_stlinkBurnAddr, 0, 3, 1, 1)

        self.chk_stlinkChipLock = QCheckBox(self.grp_stlink)
        self.chk_stlinkChipLock.setObjectName(u"chk_stlinkChipLock")

        self.gridLayout_26.addWidget(self.chk_stlinkChipLock, 0, 5, 1, 1)

        self.brn_stlinkChipUnLock = QPushButton(self.grp_stlink)
        self.brn_stlinkChipUnLock.setObjectName(u"brn_stlinkChipUnLock")
        self.brn_stlinkChipUnLock.setMinimumSize(QSize(100, 30))

        self.gridLayout_26.addWidget(self.brn_stlinkChipUnLock, 0, 8, 1, 1)


        self.gridLayout_15.addLayout(self.gridLayout_26, 2, 0, 1, 1)


        self.gridLayout_28.addWidget(self.grp_stlink, 1, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_28.addItem(self.verticalSpacer, 2, 0, 1, 1)

        self.tab_func.addTab(self.FactoryBurn, "")

        self.gridLayout_7.addWidget(self.tab_func, 1, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 970, 22))
        self.menu_2 = QMenu(self.menubar)
        self.menu_2.setObjectName(u"menu_2")
        self.menu_3 = QMenu(self.menubar)
        self.menu_3.setObjectName(u"menu_3")
        MainWindow.setMenuBar(self.menubar)

        self.menubar.addAction(self.menu_2.menuAction())
        self.menubar.addAction(self.menu_3.menuAction())
        self.menu_2.addAction(self.menu_helpDirection)
        self.menu_2.addAction(self.menu_helpAbout)
        self.menu_3.addAction(self.action_2)
        self.menu_3.addAction(self.action_3)

        self.retranslateUi(MainWindow)

        self.tab_func.setCurrentIndex(0)
        self.cmb_jlinkChipType.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"LiSunTool", None))
        self.action_2.setText(QCoreApplication.translate("MainWindow", u"\u4e2d\u6587", None))
        self.action_3.setText(QCoreApplication.translate("MainWindow", u"\u82f1\u6587", None))
        self.menu_helpDirection.setText(QCoreApplication.translate("MainWindow", u"\u4f7f\u7528\u8bf4\u660e", None))
        self.menu_helpAbout.setText(QCoreApplication.translate("MainWindow", u"\u5173\u4e8e", None))
        self.cmb_serialNum.setItemText(0, QCoreApplication.translate("MainWindow", u"COM1", None))

        self.lbl_serialNum.setText(QCoreApplication.translate("MainWindow", u"\u4e32\u53e3\u53f7\uff1a", None))
        self.btn_switchSerial.setText(QCoreApplication.translate("MainWindow", u"\u8fde\u63a5", None))
        self.btn_scanSerial.setText(QCoreApplication.translate("MainWindow", u"\u626b\u63cf", None))
        self.grp_upDfu.setTitle(QCoreApplication.translate("MainWindow", u"\u56fa\u4ef6\u5347\u7ea7", None))
        self.lbl_upFilePath.setText(QCoreApplication.translate("MainWindow", u"\u6587\u4ef6\u8def\u5f84\uff1a", None))
        self.brn_upFilePath.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u6587\u4ef6", None))
        self.brn_upStartUpgrade.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u5347\u7ea7", None))
        ___qtreewidgetitem = self.twd_otherType.headerItem()
        ___qtreewidgetitem.setText(1, QCoreApplication.translate("MainWindow", u"\u503c", None));
        ___qtreewidgetitem.setText(0, QCoreApplication.translate("MainWindow", u"\u5176\u4ed6\u7c7b\u578b", None));

        __sortingEnabled = self.twd_otherType.isSortingEnabled()
        self.twd_otherType.setSortingEnabled(False)
        ___qtreewidgetitem1 = self.twd_otherType.topLevelItem(0)
        ___qtreewidgetitem1.setText(1, QCoreApplication.translate("MainWindow", u"100", None));
        ___qtreewidgetitem1.setText(0, QCoreApplication.translate("MainWindow", u"PWM\u57fa\u7840\u503c", None));
        ___qtreewidgetitem2 = self.twd_otherType.topLevelItem(1)
        ___qtreewidgetitem2.setText(1, QCoreApplication.translate("MainWindow", u"75", None));
        ___qtreewidgetitem2.setText(0, QCoreApplication.translate("MainWindow", u"\u6e29\u63a7\u89e6\u53d1\u503c", None));
        ___qtreewidgetitem3 = self.twd_otherType.topLevelItem(2)
        ___qtreewidgetitem3.setText(1, QCoreApplication.translate("MainWindow", u"50", None));
        ___qtreewidgetitem3.setText(0, QCoreApplication.translate("MainWindow", u"\u6e29\u63a7\u6b63\u5e38\u503c", None));
        self.twd_otherType.setSortingEnabled(__sortingEnabled)

        ___qtreewidgetitem4 = self.twd_lightType.headerItem()
        ___qtreewidgetitem4.setText(1, QCoreApplication.translate("MainWindow", u"\u503c", None));
        ___qtreewidgetitem4.setText(0, QCoreApplication.translate("MainWindow", u"\u706f\u5149\u7c7b\u578b", None));

        __sortingEnabled1 = self.twd_lightType.isSortingEnabled()
        self.twd_lightType.setSortingEnabled(False)
        ___qtreewidgetitem5 = self.twd_lightType.topLevelItem(0)
        ___qtreewidgetitem5.setText(0, QCoreApplication.translate("MainWindow", u"Mode", None));
        ___qtreewidgetitem6 = ___qtreewidgetitem5.child(0)
        ___qtreewidgetitem6.setText(1, QCoreApplication.translate("MainWindow", u"0-10", None));
        ___qtreewidgetitem6.setText(0, QCoreApplication.translate("MainWindow", u"Gears", None));
        ___qtreewidgetitem7 = ___qtreewidgetitem5.child(1)
        ___qtreewidgetitem7.setText(1, QCoreApplication.translate("MainWindow", u"0-50", None));
        ___qtreewidgetitem7.setText(0, QCoreApplication.translate("MainWindow", u"Gears", None));
        ___qtreewidgetitem8 = ___qtreewidgetitem5.child(2)
        ___qtreewidgetitem8.setText(1, QCoreApplication.translate("MainWindow", u"0-100", None));
        ___qtreewidgetitem8.setText(0, QCoreApplication.translate("MainWindow", u"Gears", None));
        ___qtreewidgetitem9 = self.twd_lightType.topLevelItem(1)
        ___qtreewidgetitem9.setText(0, QCoreApplication.translate("MainWindow", u"Mode", None));
        ___qtreewidgetitem10 = ___qtreewidgetitem9.child(0)
        ___qtreewidgetitem10.setText(1, QCoreApplication.translate("MainWindow", u"1-10-100", None));
        ___qtreewidgetitem10.setText(0, QCoreApplication.translate("MainWindow", u"Gears", None));
        ___qtreewidgetitem11 = ___qtreewidgetitem9.child(1)
        ___qtreewidgetitem11.setText(1, QCoreApplication.translate("MainWindow", u"1-30-200", None));
        ___qtreewidgetitem11.setText(0, QCoreApplication.translate("MainWindow", u"Gears", None));
        ___qtreewidgetitem12 = ___qtreewidgetitem9.child(2)
        ___qtreewidgetitem12.setText(1, QCoreApplication.translate("MainWindow", u"1-100-100", None));
        ___qtreewidgetitem12.setText(0, QCoreApplication.translate("MainWindow", u"Gears", None));
        self.twd_lightType.setSortingEnabled(__sortingEnabled1)

        self.lbl_lampCfgHelp.setText(QCoreApplication.translate("MainWindow", u"\u706f\u5149\u503c:(\u706f\u5149\u7c7b\u578b)-(\u706f\u5149\u4eae\u5ea6)-(\u706f\u5149\u5468\u671f)\n"
"\u706f\u5149\u7c7b\u578b:\n"
"0=\u5173\u706f    1=\u5e38\u4eae      2=\u547c\u5438      3= \u4e3b\u706f\u547c\u5438    4=\u95ea\u70c1\n"
"5=SOS     6=\u7f13\u6162\u4e0a\u5347  6=\u7f13\u6162\u4e0b\u964d\n"
"\n"
"\u706f\u5149\u4eae\u5ea6\uff1a0-100\n"
"\u706f\u5149\u5468\u671f\uff1a0-65535(ms)", None))
        self.btn_deleteGear.setText(QCoreApplication.translate("MainWindow", u"\u5220\u9664\u6863\u4f4d", None))
        self.btn_addMode.setText(QCoreApplication.translate("MainWindow", u"\u589e\u52a0\u6a21\u5f0f", None))
        self.btn_addGear.setText(QCoreApplication.translate("MainWindow", u"\u589e\u52a0\u6863\u4f4d", None))
        self.btn_deleteMode.setText(QCoreApplication.translate("MainWindow", u"\u5220\u9664\u6a21\u5f0f", None))
        self.btn_writeConfig.setText(QCoreApplication.translate("MainWindow", u"\u5199\u5165\u914d\u7f6e", None))
        self.btn_readConfig.setText(QCoreApplication.translate("MainWindow", u"\u8bfb\u53d6\u914d\u7f6e", None))
        self.btn_refreshLampInfo.setText(QCoreApplication.translate("MainWindow", u"\u5237\u65b0\u8f66\u706f\u4fe1\u606f", None))
        self.grp_lampInfo.setTitle(QCoreApplication.translate("MainWindow", u"\u8f66\u706f\u4fe1\u606f", None))
        self.lbl_lampInfo1.setText(QCoreApplication.translate("MainWindow", u"\u4ea7\u54c1\u540d\u79f0\uff1a", None))
        self.lbl_lampInfoProductName.setText("")
        self.lbl_lampInfo1_2.setText(QCoreApplication.translate("MainWindow", u"\u56fa\u4ef6\u7248\u672c\uff1a", None))
        self.lbl_lampInfoFirmwareVer.setText("")
        self.lbl_lampInfo1_3.setText(QCoreApplication.translate("MainWindow", u"\u56fa\u4ef6\u65e5\u671f\uff1a", None))
        self.lbl_lampInfoFirmwareDate.setText("")
        self.lbl_lampInfo1_4.setText(QCoreApplication.translate("MainWindow", u"\u5236\u9020\u5546\uff1a", None))
        self.lbl_lampInfoMFR.setText("")
        self.lbl_lampInfo1_5.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u72b6\u6001\uff1a", None))
        self.lbl_lampInfoState.setText("")
        self.lbl_lampInfo1_6.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u6a21\u5f0f\uff1a", None))
        self.lbl_lampInfoMode.setText("")
        self.lbl_lampInfo1_7.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u6863\u4f4d\uff1a", None))
        self.lbl_lampInfoGrade.setText("")
        self.lbl_lampInfo1_8.setText(QCoreApplication.translate("MainWindow", u"\u9501\u6863\u72b6\u6001\uff1a", None))
        self.lbl_lampInfoLock.setText("")
        self.lbl_lampInfo1_9.setText(QCoreApplication.translate("MainWindow", u"\u767d\u5929/\u9ed1\u591c\uff1a", None))
        self.lbl_lampInfoDayNight.setText("")
        self.lbl_lampInfo1_10.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u7535\u91cf\uff1a", None))
        self.lbl_lampInfoBatSoc.setText("")
        self.lbl_lampInfo1_11.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u7535\u538b\uff1a", None))
        self.lbl_lampInfoBatVol.setText("")
        self.lbl_lampInfo1_12.setText(QCoreApplication.translate("MainWindow", u"\u5145\u6ee1\u7535\u538b\uff1a", None))
        self.lbl_lampInfoBatFullVol.setText("")
        self.lbl_lampInfoBatState.setText("")
        self.lbl_lampInfo1_14.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u7535\u6c60\u72b6\u6001\uff1a", None))
        self.lbl_lampInfoLuminPct.setText("")
        self.lbl_lampInfo1_16.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u706f\u5149%\uff1a", None))
        self.lbl_lampInfoTemp.setText("")
        self.lbl_lampInfo1_17.setText(QCoreApplication.translate("MainWindow", u"\u5f53\u524d\u6e29\u5ea6\uff1a", None))
        self.tab_func.setTabText(self.tab_func.indexOf(self.user), QCoreApplication.translate("MainWindow", u"\u7528\u6237\u533a", None))
        self.pte_debugReceive.setPlainText("")

        __sortingEnabled2 = self.lvw_debugAT.isSortingEnabled()
        self.lvw_debugAT.setSortingEnabled(False)
        ___qlistwidgetitem = self.lvw_debugAT.item(0)
        ___qlistwidgetitem.setText(QCoreApplication.translate("MainWindow", u"\u67e5\u8be2\u7248\u672c", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem.setToolTip(QCoreApplication.translate("MainWindow", u"AT^VERSION?\n"
"\n"
"\u67e5\u8be2\u7248\u672c", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem1 = self.lvw_debugAT.item(1)
        ___qlistwidgetitem1.setText(QCoreApplication.translate("MainWindow", u"\u8fdb\u5165BootLoader", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem1.setToolTip(QCoreApplication.translate("MainWindow", u"bootloader\n"
"\n"
"\u8fdb\u5165boot\u8fdb\u884c\u5347\u7ea7", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem2 = self.lvw_debugAT.item(2)
        ___qlistwidgetitem2.setText(QCoreApplication.translate("MainWindow", u"\u8f6f\u590d\u4f4d", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem2.setToolTip(QCoreApplication.translate("MainWindow", u"AT^RESET?\n"
"\n"
"\u8f6f\u91cd\u542f", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem3 = self.lvw_debugAT.item(3)
        ___qlistwidgetitem3.setText(QCoreApplication.translate("MainWindow", u"\u67e5\u770bSN\u7801", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem3.setToolTip(QCoreApplication.translate("MainWindow", u"AT^SN?\n"
"\n"
"SN\u7801\uff1a16\u5b57\u8282", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem4 = self.lvw_debugAT.item(4)
        ___qlistwidgetitem4.setText(QCoreApplication.translate("MainWindow", u"\u67e5\u8be2\u7535\u6c60\u4fe1\u606f", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem4.setToolTip(QCoreApplication.translate("MainWindow", u"AT^POWERINFO?\n"
"\n"
"\u67e5\u8be2\u7535\u6e90\u4fe1\u606f\uff1ausb\u72b6\u6001,\u662f\u5426\u5145\u6ee1\uff0c\u7535\u6c60\u72b6\u6001\uff0c\u7535\u6c60SOC\uff0c\u7535\u6c60\u7535\u538b\uff0c\n"
"\u6ee1\u7535\u7535\u538b\uff0coffset", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem5 = self.lvw_debugAT.item(5)
        ___qlistwidgetitem5.setText(QCoreApplication.translate("MainWindow", u"\u7cfb\u7edf\u4fe1\u606f", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem5.setToolTip(QCoreApplication.translate("MainWindow", u"AT^SYSINFO?\n"
"\n"
"\u7cfb\u7edf\u4fe1\u606f\uff1a\u8fd0\u884c\u72b6\u6001\uff0c\u706f\u5149\u6a21\u5f0f\uff0c\u706f\u5149\u6863\u4f4d\uff0c\u662f\u5426\u9501\u6863\uff0c\u767d\u5929/\u9ed1\u591c", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem6 = self.lvw_debugAT.item(6)
        ___qlistwidgetitem6.setText(QCoreApplication.translate("MainWindow", u"\u8bfb\u8f66\u706f\u914d\u7f6e", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem6.setToolTip(QCoreApplication.translate("MainWindow", u"AT^LAMPCFG?\n"
"\n"
"\u8bfb\u8f66\u706f\u914d\u7f6e\uff1a(\u6a21\u5f0f)-(\u6863\u4f4d)-(\u706f\u5149\u7c7b\u578b)-(\u706f\u5149\u4eae\u5ea6)-(\u706f\u5149\u5468\u671f)", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem7 = self.lvw_debugAT.item(7)
        ___qlistwidgetitem7.setText(QCoreApplication.translate("MainWindow", u"\u5199\u8f66\u706f\u914d\u7f6e", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem7.setToolTip(QCoreApplication.translate("MainWindow", u"AT^LAMPCFG=\n"
"\n"
"AT^LAMPCFG=(\u6a21\u5f0f)-(\u6863\u4f4d)-(\u706f\u5149\u7c7b\u578b)-(\u706f\u5149\u4eae\u5ea6)-(\u706f\u5149\u5468\u671f)\n"
"\u6a21\u5f0f\uff1a0-2\n"
"\u6863\u4f4d\uff1a0-8\n"
"\u706f\u5149\u7c7b\u578b\uff1a0-10\n"
"\u706f\u5149\u4eae\u5ea6\uff1a0-100\n"
"\u706f\u5149\u5468\u671f\uff1a0-65535(ms)\n"
"\u5e38\u4eae\u6a21\u5f0f\u65f6\uff0c\u706f\u5149\u5468\u671f\u65e0\u6548", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem8 = self.lvw_debugAT.item(8)
        ___qlistwidgetitem8.setText(QCoreApplication.translate("MainWindow", u"\u8bfb\u53d6\u706f\u5934\u6e29\u5ea6\u503c", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem8.setToolTip(QCoreApplication.translate("MainWindow", u"AT^LAMPTEMPERATURE?\n"
"\n"
"\u8bfb\u53d6\u706f\u5934\u6e29\u5ea6", None));
#endif // QT_CONFIG(tooltip)
        ___qlistwidgetitem9 = self.lvw_debugAT.item(9)
        ___qlistwidgetitem9.setText(QCoreApplication.translate("MainWindow", u"\u8bfb\u53d6\u5f53\u524d\u706f\u5149\u4eae\u5ea6", None));
#if QT_CONFIG(tooltip)
        ___qlistwidgetitem9.setToolTip(QCoreApplication.translate("MainWindow", u"AT^LAMPPWMOUT?\n"
"\n"
"\u8bfb\u53d6\u5f53\u524d\u706f\u5149\u4eae\u5ea6", None));
#endif // QT_CONFIG(tooltip)
        self.lvw_debugAT.setSortingEnabled(__sortingEnabled2)

#if QT_CONFIG(tooltip)
        self.lvw_debugAT.setToolTip("")
#endif // QT_CONFIG(tooltip)
        ___qtablewidgetitem = self.tbw_loopDebugAT.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("MainWindow", u"AT\u547d\u4ee4", None));
        ___qtablewidgetitem1 = self.tbw_loopDebugAT.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("MainWindow", u"\u95f4\u9694\u65f6\u95f4(ms)", None));
        ___qtablewidgetitem2 = self.tbw_loopDebugAT.verticalHeaderItem(0)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("MainWindow", u"1", None));
        ___qtablewidgetitem3 = self.tbw_loopDebugAT.verticalHeaderItem(1)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("MainWindow", u"2", None));
        ___qtablewidgetitem4 = self.tbw_loopDebugAT.verticalHeaderItem(2)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("MainWindow", u"3", None));
        ___qtablewidgetitem5 = self.tbw_loopDebugAT.verticalHeaderItem(3)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("MainWindow", u"4", None));
        ___qtablewidgetitem6 = self.tbw_loopDebugAT.verticalHeaderItem(4)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("MainWindow", u"5", None));
        ___qtablewidgetitem7 = self.tbw_loopDebugAT.verticalHeaderItem(5)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("MainWindow", u"6", None));

        __sortingEnabled3 = self.tbw_loopDebugAT.isSortingEnabled()
        self.tbw_loopDebugAT.setSortingEnabled(False)
        ___qtablewidgetitem8 = self.tbw_loopDebugAT.item(0, 0)
        ___qtablewidgetitem8.setText(QCoreApplication.translate("MainWindow", u"AT^VERSION?", None));
        ___qtablewidgetitem9 = self.tbw_loopDebugAT.item(0, 1)
        ___qtablewidgetitem9.setText(QCoreApplication.translate("MainWindow", u"100", None));
        ___qtablewidgetitem10 = self.tbw_loopDebugAT.item(1, 0)
        ___qtablewidgetitem10.setText(QCoreApplication.translate("MainWindow", u"AT^SN?", None));
        ___qtablewidgetitem11 = self.tbw_loopDebugAT.item(1, 1)
        ___qtablewidgetitem11.setText(QCoreApplication.translate("MainWindow", u"100", None));
        ___qtablewidgetitem12 = self.tbw_loopDebugAT.item(2, 0)
        ___qtablewidgetitem12.setText(QCoreApplication.translate("MainWindow", u"AT^POWERINFO?", None));
        ___qtablewidgetitem13 = self.tbw_loopDebugAT.item(2, 1)
        ___qtablewidgetitem13.setText(QCoreApplication.translate("MainWindow", u"100", None));
        ___qtablewidgetitem14 = self.tbw_loopDebugAT.item(3, 0)
        ___qtablewidgetitem14.setText(QCoreApplication.translate("MainWindow", u"AT^SYSINFO?", None));
        ___qtablewidgetitem15 = self.tbw_loopDebugAT.item(3, 1)
        ___qtablewidgetitem15.setText(QCoreApplication.translate("MainWindow", u"100", None));
        ___qtablewidgetitem16 = self.tbw_loopDebugAT.item(4, 0)
        ___qtablewidgetitem16.setText(QCoreApplication.translate("MainWindow", u"AT^POWERINFO?", None));
        ___qtablewidgetitem17 = self.tbw_loopDebugAT.item(4, 1)
        ___qtablewidgetitem17.setText(QCoreApplication.translate("MainWindow", u"100", None));
        ___qtablewidgetitem18 = self.tbw_loopDebugAT.item(5, 0)
        ___qtablewidgetitem18.setText(QCoreApplication.translate("MainWindow", u"AT^LAMPCFG?", None));
        ___qtablewidgetitem19 = self.tbw_loopDebugAT.item(5, 1)
        ___qtablewidgetitem19.setText(QCoreApplication.translate("MainWindow", u"100", None));
        self.tbw_loopDebugAT.setSortingEnabled(__sortingEnabled3)

        self.lbl_loopHelp.setText(QCoreApplication.translate("MainWindow", u"\u6ce8:-1\u4ee3\u8868\u65e0\u9650", None))
        self.lbl_residueDegree_3.setText(QCoreApplication.translate("MainWindow", u"\u63a5\u6536\u6570:0", None))
        self.le_loopDebugSendNum.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.lbl_loopDebugSendNum_5.setText(QCoreApplication.translate("MainWindow", u"\u53d1\u9001\u6b21\u6570:", None))
        self.lbl_residueDegree_2.setText(QCoreApplication.translate("MainWindow", u"\u53d1\u9001\u6570:0", None))
        self.lbl_residueDegree.setText(QCoreApplication.translate("MainWindow", u"\u5269\u4f59\u6570:0", None))
        self.btn_debugCleanSendText.setText(QCoreApplication.translate("MainWindow", u"\u6e05\u9664\u53d1\u9001", None))
        self.btn_debugSend.setText(QCoreApplication.translate("MainWindow", u"\u53d1\u9001", None))
        self.btn_debugCleanReceiveText.setText(QCoreApplication.translate("MainWindow", u"\u6e05\u9664\u63a5\u6536", None))
        self.btn_debugSaveReceiveText.setText(QCoreApplication.translate("MainWindow", u"\u4fdd\u5b58\u5185\u5bb9", None))
        self.btn_loopDebugStart.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u5faa\u73af", None))
        self.btn_loopAddLine.setText(QCoreApplication.translate("MainWindow", u"\u589e\u52a0\u4e00\u884c", None))
        self.btn_loopDelLine.setText(QCoreApplication.translate("MainWindow", u"\u5220\u9664\u4e00\u884c", None))
        self.btn_loopDebugFull.setText(QCoreApplication.translate("MainWindow", u"\u5168\u9009", None))
        self.btn_loopDebugNop.setText(QCoreApplication.translate("MainWindow", u"\u53d6\u6d88\u5168\u9009", None))
        self.tab_func.setTabText(self.tab_func.indexOf(self.debug), QCoreApplication.translate("MainWindow", u"\u8c03\u8bd5\u533a", None))
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"\u64cd\u4f5c\u533a", None))
        self.btn_ghStartRecord.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u8bb0\u5f55", None))
        self.btn_ghClearData.setText(QCoreApplication.translate("MainWindow", u"\u6e05\u9664\u6570\u636e", None))
        self.btn_ghSavaData.setText(QCoreApplication.translate("MainWindow", u"\u4fdd\u5b58\u6570\u636e", None))
        self.tab_func.setTabText(self.tab_func.indexOf(self.graph), QCoreApplication.translate("MainWindow", u"\u66f2\u7ebf\u56fe", None))
        self.grp_jlink.setTitle(QCoreApplication.translate("MainWindow", u"JLink", None))
        self.brn_jlinkFolderPath.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u76ee\u5f55", None))
        self.lbl_jlinkFilePath.setText(QCoreApplication.translate("MainWindow", u"JLink\u8def\u5f84\uff1a", None))
        self.brn_jlinkFindPath.setText(QCoreApplication.translate("MainWindow", u"\u81ea\u52a8\u641c\u7d22", None))
        self.le_jlinkBurnAddr.setText(QCoreApplication.translate("MainWindow", u"0x0", None))
        self.chk_jlinkChipLock.setText(QCoreApplication.translate("MainWindow", u"\u82af\u7247\u4e0a\u9501", None))
        self.cmb_jlinkChipType.setItemText(0, QCoreApplication.translate("MainWindow", u"CX32L003", None))
        self.cmb_jlinkChipType.setItemText(1, QCoreApplication.translate("MainWindow", u"NRF51422", None))
        self.cmb_jlinkChipType.setItemText(2, QCoreApplication.translate("MainWindow", u"NRF51832", None))
        self.cmb_jlinkChipType.setItemText(3, QCoreApplication.translate("MainWindow", u"NRF52810", None))
        self.cmb_jlinkChipType.setItemText(4, QCoreApplication.translate("MainWindow", u"NRF52820", None))
        self.cmb_jlinkChipType.setItemText(5, QCoreApplication.translate("MainWindow", u"NRF52832", None))
        self.cmb_jlinkChipType.setItemText(6, QCoreApplication.translate("MainWindow", u"NRF52840", None))

        self.lbl_jlinkChipType.setText(QCoreApplication.translate("MainWindow", u"\u82af\u7247\u578b\u53f7\uff1a", None))
        self.brn_jlinkStartBurnFile.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u70e7\u5f55", None))
        self.lbl_jlinkBurnAddr.setText(QCoreApplication.translate("MainWindow", u"\u70e7\u5f55\u5730\u5740\uff1a", None))
        self.brn_jlinkChipUnLock.setText(QCoreApplication.translate("MainWindow", u"\u82af\u7247\u89e3\u9501", None))
        self.lbl_jlinkBurnFilePath.setText(QCoreApplication.translate("MainWindow", u"\u70e7\u5f55\u6587\u4ef6\u8def\u5f84\uff1a", None))
        self.brn_jlinkBurnFilePath.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u6587\u4ef6", None))
        self.grp_stlink.setTitle(QCoreApplication.translate("MainWindow", u"STLink", None))
        self.brn_stlinkFolderPath.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u76ee\u5f55", None))
        self.lbl_stlinkFilePath.setText(QCoreApplication.translate("MainWindow", u"STLink\u8def\u5f84\uff1a", None))
        self.brn_stlinkFindPath.setText(QCoreApplication.translate("MainWindow", u"\u81ea\u52a8\u641c\u7d22", None))
        self.lbl_stlinkBurnFilePath.setText(QCoreApplication.translate("MainWindow", u"\u70e7\u5f55\u6587\u4ef6\u8def\u5f84\uff1a", None))
        self.brn_stlinkBurnFilePath.setText(QCoreApplication.translate("MainWindow", u"\u9009\u62e9\u6587\u4ef6", None))
        self.le_stlinkBurnAddr.setText(QCoreApplication.translate("MainWindow", u"0x0", None))
        self.brn_stlinkStartBurnFile.setText(QCoreApplication.translate("MainWindow", u"\u5f00\u59cb\u70e7\u5f55", None))
        self.lbl_stlinkChipType.setText(QCoreApplication.translate("MainWindow", u"\u82af\u7247\u578b\u53f7\uff1a", None))
        self.cmb_stlinkChipType.setItemText(0, QCoreApplication.translate("MainWindow", u"CX32L003", None))
        self.cmb_stlinkChipType.setItemText(1, QCoreApplication.translate("MainWindow", u"NRF51422", None))
        self.cmb_stlinkChipType.setItemText(2, QCoreApplication.translate("MainWindow", u"NRF51832", None))
        self.cmb_stlinkChipType.setItemText(3, QCoreApplication.translate("MainWindow", u"NRF52810", None))
        self.cmb_stlinkChipType.setItemText(4, QCoreApplication.translate("MainWindow", u"NRF52820", None))
        self.cmb_stlinkChipType.setItemText(5, QCoreApplication.translate("MainWindow", u"NRF52832", None))
        self.cmb_stlinkChipType.setItemText(6, QCoreApplication.translate("MainWindow", u"NRF52840", None))

        self.lbl_stlinkBurnAddr.setText(QCoreApplication.translate("MainWindow", u"\u70e7\u5f55\u5730\u5740\uff1a", None))
        self.chk_stlinkChipLock.setText(QCoreApplication.translate("MainWindow", u"\u82af\u7247\u4e0a\u9501", None))
        self.brn_stlinkChipUnLock.setText(QCoreApplication.translate("MainWindow", u"\u82af\u7247\u89e3\u9501", None))
        self.tab_func.setTabText(self.tab_func.indexOf(self.FactoryBurn), QCoreApplication.translate("MainWindow", u"\u5de5\u5382\u70e7\u5f55", None))
        self.menu_2.setTitle(QCoreApplication.translate("MainWindow", u"\u5e2e\u52a9", None))
        self.menu_3.setTitle(QCoreApplication.translate("MainWindow", u"\u8bed\u8a00", None))
    # retranslateUi

