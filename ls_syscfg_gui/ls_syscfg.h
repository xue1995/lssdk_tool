/********************************************************************************
* @file    ls_syscfg.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-03
* @brief   业务类--功能宏定义
********************************************************************************/

#ifndef __LS_SYSCFG_H
#define __LS_SYSCFG_H


//-------- <<< Use Configuration Wizard in Context Menu >>> -----------------

/**************************************LOG**************************************/
// <e> Log Switch
// =======================
#define LS_LOG_SWITCH                                              (0x1)

//  <o> Log Grade
//  <i> Default: 0
//      <0=> ALL
//      <1=> TRACE
//      <2=> DEBUG
//      <3=> INFO
//      <4=> WARN
//      <5=> ERROR
//      <6=> FATAL
//      <7=> OFF
#define LS_LOG_GRADE                                               (0x2)

//  <o> Log Out
//  <i> Default: 0
//      <0=> RTT
//      <1=> UART
#define LS_LOG_OUT                                                 (0x1)
// </e>

/************************************FLASH**************************************/
// <h> FLASH
// =======================
//
//  <o> Page Size
//  <i> Default: 0x200 (512 byte)
#define LS_FLASH_PAGE_SIZE                                         (0x200U)

//  <o> Flash Max Size
//  <i> Default: 0x10000 (64K byte)
#define LS_FLASH_MAX_SIZE                                          (0x10000U)

//  <o> Boot Loader Starting Address
//  <i> Default: 0
#define LS_FLASH_START_ADDR                                        (0x0000000)

//  <o> Boot Loader Size
//  <i> Default: 0x1400 (5K byte), use dfu 0x2000
#define LS_FLASH_BOOT_SIZE                                         (0x2000)

//  <o> Kv Flash Page
//  <i> Default: 2
#define LS_FLASH_KV_PAGE                                           (3)

//  <o> Kv One Item Byte
//  <i> Default: 32
#define LS_FLASH_KV_ONE_PAGE_BYTE                                  (32)

//  <o> FLASH_INFO_TYPE(MCU user Kv_Sys / NRF user FDS)
//      <0=> KV_SYS
//      <1=> FDS
#define LS_FLASH_INFO_TYPE                                         (0)

// </h>

/*************************************APP_IO************************************/
// <h> APP_IO
// =======================
//  <q> save flash support
#define LS_IO_FLAG_SAVE_SUPPORT                                    (0)

//  <q> active report support (ATCMD enevt)
#define LS_IO_FLAG_ACTIVE_REPORT_SUPPORT                           (0)

//  <q> key scan support
#define LS_IO_FLAG_KEY_SCAN_SUPPORT                                (1)

//  <q> matrix key support
#define LS_IO_FLAG_MATRIX_KEY_SUPPORT                              (0)

//  <e> period task support
#define LS_IO_FLAG_PERIOD_TASK_SUPPORT                             (1)
//   <o> period task num
#define LS_IO_FLAG_PERIOD_TASK_NUM                                 (2)
//   <o> period task time (ms)
#define LS_IO_FLAG_PERIOD_TASK_TIME_MS                             (1000)
//  </e>
//  <q> swo_off
#define LS_IO_FLAG_SWO_OFF                                         (0)
//  <q> extend_support
#define LS_IO_EXTEND_SUPPORT                                       (0)
// </h>
/**************************************KEY**************************************/
// <h> KEY
// =======================
//  <o> Button scan cycle (ms) <10-65535>
//  <i> Default: 10
#define LS_BUTTON_SCAN_CYCLE_MS                                    (10)

//  <o> Single click time(ms) <0-65535>
//  <i> 1 timec of one click
//  <i> Default: 100
#define LS_BUTTON_SINGLE_CLICK_TIME                                (50)

//  <o> Release time(ms) <0-65535>
//  <i> press -> release
//  <i> Default: 100
#define LS_BUTTON_RELEASE_TIME                                     (140)

//  <o> Double click time(ms) <0-65535>
//  <i> 1 timec of double click interval time
//  <i> Default: 150
#define LS_BUTTON_DOUBLE_CLICK_TIME                                (200)

//  <o> Long press time(ms) <100-65535>
//  <i> Long press
//  <i> Default: 1000
#define LS_BUTTON_LONG_PRESS_TIME                                  (800)

//  <o> Long long press time(ms) <1000-65535>
//  <i> Long long press
//  <i> Default: 1000
#define LS_BUTTON_LONG_LONG_PRESS_TIME                             (10000)

//   <q> Automatic Scanning(Idle Sop, Interrupt Activation)
//   <i> Whether to automatically disable scanning
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_BUTTON_AUTOMATIC_SCANNING                               (0)

// </h>

/**************************************ADC**************************************/
// <h> ADC
// =======================
//
//  <e> ADC0
#define LS_ADC0_EN                                                 (0)
//   <o> Sampling times
//   <i> Default: 10
#define LS_ADC0_SAMPLING_TIMES                                     (10)

//   <q> ADC0 IRQ
#define LS_ADC0_IRQ_SEL                                            (1)

//   <e1.7> ADC0_CH
//    <o0.0> ADC0_CH0
//    <o0.1> ADC0_CH1
//    <o0.2> ADC0_CH2
//    <o0.3> ADC0_CH3
//    <o0.4> ADC0_CH4
//    <o0.5> ADC0_CH5
//    <o0.6> ADC0_CH6
//    <o0.7> ADC0_CH7 (sys_vcc)
#define LS_ADC0_CH                                                 (129)
//   </e>
//  </e>

//  <h> Resistance Information
//   <h> Thermistor

//    <o> Pull Up Resistance Value
//    <i> Default: 100
//    <i> Unit: KOhm
#define LS_THERMISTOR_PULL_UP_VAL                                  (100)

//    <o> Coefficient corresponding to NTC resistance
//    <i> Default: 0 (x*1000)
#define LS_THERMISTOR_NTC_K                                        (4250)

//    <o> Reference voltage
//    <i> Default: 3300 (x*1000)
#define LS_THERMISTOR_REFERENCE_VOLTAGE                            (2700)
//    </h>

//   <h> LDR
//   <i> Photosensitive resistance
//    <o> Pull Up Resistance Value
//    <i> Default: 680
//    <i> Unit: KOhm
#define LS_LDR_PULL_UP_VAL                                         (680)

//    <o> Reference voltage
//    <i> Default: 3300 (x*1000)
#define LS_LDR_REFERENCE_VOLTAGE                                   (2700)
//    </h>

//   <h> Power resistance
//   <i> Power supply resistance
//    <o> Pull Up Resistance Value
//    <i> Default: 300000
//    <i> Unit: KOhm
#define LS_POWER_PULL_UP_VAL                                       (300)

//    <o> Pull Down Resistance Value
//    <i> Default: 100
//    <i> Unit: KOhm
#define LS_POWER_PULL_DOWN_VAL                                     (300)

//    <o> Reference voltage
//    <i> Default: 3300 (x*1000)
#define LS_POWER_REFERENCE_VOLTAGE                                 (2700)
//   </h>
//  </h>
// </h>

/**************************************I2C**************************************/
// <h> I2C
// =======================
//  <e> I2C0
#define LS_I2C0_EN                                                 (0)

//   <o> I2C0 SPEED uint khz
//   <i> Default: 100 khz
#define LS_I2C0_SPEED_RATE                                         (400)
//  </e>

//  <e> I2C1
#define LS_I2C1_EN                                                 (0)

//   <o> I2C1 SPEED uint khz
//   <i> Default: 100 khz
#define LS_I2C1_SPEED_RATE                                         (100)
//  </e>

//  <e> I2C2
#define LS_I2C2_EN                                                 (0)

//   <o> I2C2 SPEED uint khz
//   <i> Default: 100 khz
#define LS_I2C2_SPEED_RATE                                         (100)
//  </e>
// </h>

/**************************************UART*************************************/
// <h> UART
// =======================
//  <e> UART0
#define LS_UART0_EN                                                (1)

//   <o> UART0 Baud Rate
//   <i> Default: 115200
//       <9600    => 9600
//       <19200   => 19200
//       <57600   => 57600
//       <115200  => 115200
#define LS_UART0_BAUD_RATE                                         (115200)

//   <o> UART0 Working mode
//   <i> Default: full duplex
//       <0  => Full Duplex
//       <1  => Half Duplex
#define LS_UART0_WORK_MODE                                         (0)

//   <o> UART0 RX cache size
//   <i> Default: 256
#define LS_UART0_CACHE_SIZE                                        (64)
//  </e>

//  <e> UART1
#define LS_UART1_EN                                                (0)

//   <o> UART1 Baud rate
//   <i> Default: 115200
//       <9600    => 9600
//       <19200   => 19200
//       <57600   => 57600
//       <115200  => 115200
#define LS_UART1_BAUD_RATE                                         (115200)

//   <o> UART1 Working mode
//   <i> Default: full duplex
//       <0  => Full Duplex
//       <1  => Half Duplex
#define LS_UART1_WORK_MODE                                         (0)

//   <o> UART1 RX cache size
//   <i> Default: 256
#define LS_UART1_CACHE_SIZE                                        (256)
//  </e>

//  <e> UART2
#define LS_UART2_EN                                                (0)

//   <o> UART2 Baud Rate
//   <i> Default: 115200
//       <9600    => 9600
//       <19200   => 19200
//       <57600   => 57600
//       <115200  => 115200
#define LS_UART2_BAUD_RATE                                         (115200)

//   <o> UART2 Working Mode
//   <i> Default: full duplex
//       <0  => Full Duplex
//       <1  => Half Duplex
#define LS_UART2_WORK_MODE                                         (0)

//   <o> UART2 RX Cache Size
//   <i> Default: 256
#define LS_UART2_CACHE_SIZE                                        (256)
//  </e>

//  <o> Printf
//  <i> Default: UART0
//      <0=> UART0
//      <1=> UART1
//      <2=> UART2
#define LS_FUART_PRINT                                             (0)

// </h>

/**************************************PWM**************************************/
// <h> PWM
// =======================
//  <e> TIM1
//  <i> pwm hz = sys_clock / (Prescaler+1) / (Period+1)
//  <i> 3125 hz = 24000000 / (63+1) / (119+1)
#define LS_TIM1_EN                                                 (1)

//   <o> Prescaler
//   <i> Default: 63
#define LS_TIM1_PRESCALER                                          (63)

//   <o> Period
//   <i> Default: 119
#define LS_TIM1_PERIOD                                             (119)

//   <o> Level logic
//   <i> Reverse --> low level light up
//   <i> Forward --> high level light up
//       <0=> Reverse
//       <1=> Forward
#define LS_TIM1_LEVEL_LOGIC                                        (1)

//   <q> TIM1_CH1 (PWM0)
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM0_EN                                                 (1)

//   <q> TIM1_CH2 (PWM1)
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM1_EN                                                 (1)

//   <q> TIM1_CH3 (PWM2)
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM2_EN                                                 (1)

//   <q> TIM1_CH4 (PWM3)
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM3_EN                                                 (1)
//  </e>

//  <e> TIM2
//  <i> pwm hz = sys_clock / (Prescaler+1) / (Period+1)
//  <i> 3125 hz = 24000000 / (63+1) / (119+1)
#define LS_TIM2_EN                                                 (0)

//   <o> Prescaler
//   <i> Default: 63
#define LS_TIM2_PRESCALER                                          (63)

//   <o> Period
//   <i> Default: 119
#define LS_TIM2_PERIOD                                             (119)

//   <o> Level logic
//   <i> Reverse --> low level light up
//   <i> Forward --> high level light up
//       <0=> Reverse
//       <1=> Forward
#define LS_TIM2_LEVEL_LOGIC                                        (1)

//   <q> TIM2_CH1 (PWM4)
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM4_EN                                                 (1)

//   <q> TIM2_CH2 (PWM5)
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM5_EN                                                 (1)

//   <q> TIM2_CH3 (PWM6) Not
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM6_EN                                                 (0)

//   <q> TIM2_CH4 (PWM7)
//   <i> Default: DISABLE
//       <0=> DISABLE
//       <1=> ENABLE
#define LS_PWM7_EN                                                 (1)
//  </e>
// </h>

/***********************************Segger_RTT**********************************/
// <h> SEGGER RTT
// =======================
// <o> SEGGER_RTT_CONFIG_BUFFER_SIZE_UP - Size of upstream buffer.
// <i> Default: 1k
#define SEGGER_RTT_CONFIG_BUFFER_SIZE_UP                           (256)

// <o> SEGGER_RTT_CONFIG_MAX_NUM_UP_BUFFERS - Size of upstream buffer.
#define SEGGER_RTT_CONFIG_MAX_NUM_UP_BUFFERS                       (3)

// <o> SEGGER_RTT_CONFIG_BUFFER_SIZE_DOWN - Size of upstream buffer.
#define SEGGER_RTT_CONFIG_BUFFER_SIZE_DOWN                         (16)

// <o> SEGGER_RTT_CONFIG_MAX_NUM_DOWN_BUFFERS - Size of upstream buffer.
#define SEGGER_RTT_CONFIG_MAX_NUM_DOWN_BUFFERS                     (3)

// <o> SEGGER_RTT_CONFIG_PRINTF_BUFFER_SIZE - Size of buffer for RTT printf to bulk-send chars via RTT
// <i> Default: 64
#define SEGGER_RTT_CONFIG_PRINTF_BUFFER_SIZE                       (128u)

// </h>
/*************************************BOOT************************************/
// <h> BootLoader
// =======================
//   <q> BOOT_SUPPORT
#define BOOT_SUPPORT                                               (1)
//   <s> chip ic
#define CHIP_IC                                                    "CX32L003"
//   <s> version info
#define VERSION_INFO                                               "0.6"
// </h>

//------------- <<< end of configuration section >>> -----------------------

/*********************************[业务引脚命令]*******************************/
#define LS_IO_LED_IND_ID                                           (11)


/********************************[FLASH INFO]*********************************/
#define LS_FLASH_APP_ADDR                                          (LS_FLASH_START_ADDR + LS_FLASH_BOOT_SIZE)
#define LS_FLASH_APP_SIZE                                          (LS_FLASH_MAX_SIZE - LS_FLASH_BOOT_SIZE - (LS_FLASH_KV_PAGE * LS_FLASH_PAGE_SIZE))
#define LS_FLASH_OTA_ADDR                                          (LS_FLASH_APP_ADDR + LS_FLASH_APP_SIZE / 2)
#define LS_FLASH_END_ADDR                                          (LS_FLASH_START_ADDR + LS_FLASH_MAX_SIZE)

#define LS_KV_BASE_ADDR                                            (LS_FLASH_END_ADDR - (LS_FLASH_KV_PAGE * LS_FLASH_PAGE_SIZE))
#define LS_KV_BACK_ADDR                                            (LS_KV_BASE_ADDR + (LS_FLASH_KV_PAGE - 1) * LS_FLASH_PAGE_SIZE)
#define LS_OPTION_BYTES_ADDR                                       (0x08000000)

#define LS_KV_KEY_BOOT_INFO                                         0x01
#define LS_KV_KEY_POWER_INFO                                        0x02
#define LS_KV_KEY_USER_INFO                                         0x03
#define LS_KV_KEY_UID                                               0x04

#define LS_KV_KEY_IO_INFO                                           0x05
#endif
