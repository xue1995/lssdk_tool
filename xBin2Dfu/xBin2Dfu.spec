# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['xBin2Dfu.py'],
             pathex=['E:\\Singlechip\\APF\\tool\\xBin2Dfu'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='xBin2Dfu',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=['msvcp140.dll', 'msvcp140_1.dll', 'vcruntime140.dll', 'python36.dll', 'vcruntime140_1.dll', 'qwindowsvistastyle.dll', 'qwindows.dll', 'qwebgl.dll', 'qoffscreen.dll', 'qminimal.dll', 'qico.dll'],
          runtime_tmpdir=None,
          console=True )
